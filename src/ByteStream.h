/**
 *   Copyright TBD by Liangfeng, All rights reserved.
 */

#pragma once

#include <stdio.h>
#include "Common.h"
class ByteStream
{
public:
    //use internal memory
    ByteStream(size_t size);
    //use external memory
    ByteStream(u_int8_t* data, size_t dataSize, size_t size);
    //Read functions
    u_int64_t   readUInt64();
    u_int32_t   readUInt32();
    u_int32_t   readUInt24();
    u_int16_t   readUInt16();
    u_int8_t    readUInt8();
    double      readDouble();
    void readString(char* buffer, int64_t size);
    const u_int8_t* getData();
    int64_t getOffset() const;
    int64_t getSize() const;
    size_t getDataSize() const { return mDataSize;}
    void resetOffset(int64_t pos = 0);

    //Write functions
    int64_t appendData(const u_int8_t* data, int64_t byteToWrite);

    virtual ~ByteStream();
protected:
    int64_t read(u_int8_t* buffer, int64_t bytesToRead);


private:
    u_int8_t*   mData;
    size_t      mDataSize;
    int64_t     mSize;
    int64_t     mOffset;
};