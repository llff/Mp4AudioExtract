/**
 *   Copyright TBD by Liangfeng, All rights reserved.
 */


#include "Mp4Crawler.h"

Mp4Crawler::Mp4Crawler(const char* url, FILE* output): mOutputFile(output), mp4Content(url)
{
    size_t len = strlen(url) > 1023 ? 1023 : strlen(url);
    strncpy(mFileUrl, url, len);
    mMediaBuffer = new u_int8_t[BUF_SIZE];

    mBufPos = 0;  // current buffer pointer
    mDownloadSize = 0;
    mSampleIndex = 0;
    mRecycleCnt = 0;
    mCopyInNext = 0;
}

Result Mp4Crawler::crawl()
{
    CURL* curl = curl_easy_init();
    if (curl == nullptr) {
        //
        return ERROR;
    }
    curl_easy_setopt(curl, CURLOPT_URL, mFileUrl);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeData);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, this);
    CURLcode res = curl_easy_perform(curl);
    if (CURLE_OK != res) {
        std::cout << "Failed to execute curl download" << std::endl;
        return ERROR;
    }
    int64_t content_len = 0;
    res = curl_easy_getinfo(curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD, &content_len);
    if ((CURLE_OK == res) && (content_len > 0.0)) {
        // std::cout << "file '"<< filename << "' size: " << filesize <<
        // std::endl;
    }
    curl_easy_cleanup(curl);
    return OK;
}

size_t Mp4Crawler::writeData(char *content, size_t size, size_t nmemb, void *userdata)
{
    Mp4Crawler* inst = (Mp4Crawler*)userdata;

    int64_t length = size * nmemb;  // Downloaded size
    int8_t* ptr = (int8_t*)content;
    inst->mDownloadSize += length;

    if (inst->mDownloadSize < BUF_SIZE) {  // wait until we have enough buffer to parse media info
        memcpy(inst->mMediaBuffer + inst->mBufPos, ptr, length);
        inst->mBufPos += length;
        return length;
    }
    if (inst->mIsFileParsed == false) {
        MoovBox* moov = nullptr;
        ByteStream byteStream(inst->mMediaBuffer, (size_t)inst->mBufPos, BUF_SIZE);
        Mp4Parser parser;
        Result result = parser.ParseMp4(byteStream, &inst->mp4Content);

        if (result != OK) {
            std::cerr << "Failed to parse media file: " << inst->mFileUrl << std::endl;
            exit(-1);
        }
        inst->mIsFileParsed = true;
        moov = inst->mp4Content.getMoovBox();
        if (moov == nullptr || moov->getTrakBoxes().size() <= 0) {
            std::cerr << "No Audio track found" << std::endl;
            exit(-1);
        }

        if (!inst->isTrackFound) {
            size_t size = moov->getTrakBoxes().size();
            TrakBox* trakBox = nullptr;
            for (auto& trk : moov->getTrakBoxes()) {
                if (trk->mHandlerType ==
                    HANDLER_AUDIO /*&& trk->mCodec == CODEC_MPEG_L3*/) {  // find audio track
                    trakBox = trk;
                    inst->isTrackFound = true;
                    break;
                }
            }
            if (trakBox != nullptr) {
                Mp4Parser::GetTrackInfo(trakBox, &inst->mTrackInfo);
            }
        }
    }

    TrackInfo* track = &inst->mTrackInfo;
    if (inst->mSampleIndex >= track->mSampleCount) {
        TRACE("No more sample found");
        exit(0);
    }

    int64_t outside_buffer = length;
    int64_t ptr_pos = 0;
    bool full_buffer = false;
    size_t space_left = BUF_SIZE - inst->mBufPos;
    if (length > space_left) {
        memcpy(inst->mMediaBuffer + inst->mBufPos, ptr, space_left);
        outside_buffer -= space_left;
        inst->mBufPos = 0;  // point to end of the buffer
        ptr_pos += space_left;
        full_buffer = true;
    } else {
        memcpy(inst->mMediaBuffer + inst->mBufPos, ptr, length);
        inst->mBufPos += length;
        outside_buffer = 0;
        full_buffer = false;
    }

    while (inst->mSampleIndex < track->mSampleCount) {
        size_t sample_size = (size_t)track->mSampleSize[inst->mSampleIndex];
        size_t sample_offset = (size_t)track->mSampleOffset[inst->mSampleIndex];
        size_t sample_end = sample_offset + sample_size;
        //printf(" | Sample index  : %d\n", inst->mSampleIndex);
        //printf(" | sample type   : %d\n", track->mSampleType[inst->mSampleIndex]);
        //printf(" | sample size   : % d\n ", sample_size); 
        //printf(" | sample offset : % d\n ", sample_offset); 
        //printf(" | downloaded    : % d\n ", inst->mDownloadSize);

        int64_t buffered_size = inst->mRecycleCnt * BUF_SIZE;

        if (full_buffer) {
            inst->mRecycleCnt++;  // recycled the buffer
            buffered_size += BUF_SIZE;
            full_buffer = false;
        } else {
            buffered_size += inst->mBufPos;
        }
        //                printf("recycle_cnt: %d\n", recycle_cnt);
        //                printf("buffered_size:  %d\n", buffered_size);

        size_t copy_pos = sample_offset % BUF_SIZE;
        if (sample_end <= buffered_size) {  // buffer has enough data
            if (inst->mCopyInNext > 0) {
                fwrite(inst->mMediaBuffer, sizeof(u_int8_t), inst->mCopyInNext, inst->mOutputFile);
                inst->mCopyInNext = 0;
            } else {
                fwrite(inst->mMediaBuffer + copy_pos, sizeof(u_int8_t), sample_size, inst->mOutputFile);
            }
            inst->mSampleIndex++;
            continue;
        }
        if (outside_buffer <= 0)  // no more in the buffer, go for next download
        {
            //TRACE(" outside_buffer <= 0 ");
            break;
        }
        if ((outside_buffer + inst->mBufPos) > BUF_SIZE)  // this means re-fill buffer
        {
            TRACE("outside_buffer + buf_pos) > BUF_SIZE");
            // part of sample in current buffer, rest of in next buffer
            int64_t space_left = BUF_SIZE - inst->mBufPos;
            memcpy(inst->mMediaBuffer + inst->mBufPos, ptr + ptr_pos, space_left);
            inst->mBufPos = 0;
            ptr_pos += space_left;
            outside_buffer -= space_left;

            // full buffer now
            if (sample_offset < inst->mRecycleCnt * BUF_SIZE) {  // part  of sample in current buffer
                size_t num = inst->mRecycleCnt * BUF_SIZE - sample_offset;
                fwrite(inst->mMediaBuffer + copy_pos, sizeof(u_int8_t), num, inst->mOutputFile);
                inst->mCopyInNext = sample_size - num;
            }

            // reset the buffer
            memset(inst->mMediaBuffer, 0, BUF_SIZE);
            inst->mBufPos = 0;
            if (outside_buffer > BUF_SIZE) {
                memcpy(inst->mMediaBuffer, ptr + ptr_pos, BUF_SIZE);
                outside_buffer -= BUF_SIZE;
                ptr_pos += BUF_SIZE;
                full_buffer = true;
            } else {
                memcpy(inst->mMediaBuffer + inst->mBufPos, ptr + ptr_pos, outside_buffer);
                inst->mBufPos += outside_buffer;
                ptr_pos += outside_buffer;
                outside_buffer = 0;
                full_buffer = false;
            }
        } else {
            memcpy(inst->mMediaBuffer + inst->mBufPos, ptr + ptr_pos, outside_buffer);
            inst->mBufPos += outside_buffer;
            ptr_pos += outside_buffer;
            outside_buffer = 0;
            full_buffer = false;
        }
    }
    return length;
}


void trace_print(const char* file,
                 const int line,
                 const char* func,
                 const unsigned level,
                 const char* payload,
                 ...)
{
    const char* level_string = "TRACE";
    fprintf(stderr, "[%s]{%s:%d}", level_string, file, line);
    fprintf(stderr, " ");

    va_list args;
    va_start(args, payload);
    vfprintf(stderr, payload, args);
    va_end(args);

    fprintf(stderr, "\n");
}