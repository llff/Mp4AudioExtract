/**
 *   Copyright TBD by Liangfeng, All rights reserved.
 */
#pragma once
#include "Common.h"
#include "Mp4Box.h"

#define MAX_URL_SIZE 2048

class Mp4Content
{
public:
    explicit Mp4Content(const char* url);
    MoovBox* getMoovBox() const;
    FtypBox* getFtypBox() const;
    void setMoovBox(MoovBox* moov);
    void setFtypBox(FtypBox* ftyp);
    ~Mp4Content();

private:
    char mFileUrl[MAX_URL_SIZE];
    MoovBox* mMoovBox;
    FtypBox* mFtypBox;
};