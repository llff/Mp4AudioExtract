/**
 *   Copyright TBD by Liangfeng, All rights reserved.
 */

#pragma once
#include <stdio.h>
#include <curl/curl.h>
#include <cstring>
#include <iostream>
#include <string>

#include "ByteStream.h"
#include "Common.h"
#include "Mp4Content.h"
#include "Mp4Parser.h"

#define BUF_SIZE (1024 * 1024 * 4)

class Mp4Crawler {

public:
    explicit Mp4Crawler(const char *url, FILE* output);
    Result crawl();

protected:
    static size_t writeData(char *ptr, size_t size, size_t nmemb, void *userdata);
private:
    char mFileUrl[1024];
    bool mIsFileParsed = false;
    bool isTrackFound = false;
    u_int8_t*   mMediaBuffer; // cyclical buffer
    int64_t     mBufPos = 0;  // current buffer pointer
    int64_t     mDownloadSize = 0;
    int64_t     mSampleIndex = 0;
    int64_t     mRecycleCnt = 0;
    int64_t     mCopyInNext = 0;
    FILE*       mOutputFile;
    //
    Mp4Content mp4Content;
    TrackInfo mTrackInfo;
};
