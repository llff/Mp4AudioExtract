/**
 *   Copyright TBD by Liangfeng, All rights reserved.
 */
#include <iostream>
#include <cstring>
#include "Common.h"
#include "ByteStream.h"

//ctor
//ByteStream::ByteStream(size_t size) :
//    mSize(size),
//    mOffset(0)
//{
//    mData = new u_int8_t[size];
//    memset(mData, 0, size);
//}


ByteStream::ByteStream(u_int8_t* data, size_t dataSize, size_t size) :
    mSize(size), 
    mOffset(0)
{
    mData = data;
    mDataSize = dataSize;
    memcpy(mData, data, size);
}
ByteStream::~ByteStream() 
{
    //
}

// Read functions
u_int64_t 
ByteStream::readUInt64()
{
    u_int8_t buffer[8];
    //TODO: Error handling
    read(buffer, 8);
    // convert bytes to value
    return BytesToUInt64(buffer);
}

u_int32_t 
ByteStream::readUInt32() 
{
    u_int8_t buffer[4];
    // TODO: Error handling
    read(buffer, 4);
    // convert bytes to value
    return BytesToUInt32(buffer);
}

u_int32_t 
ByteStream::readUInt24() 
{
    u_int8_t buffer[3];
    // TODO: Error handling
    read(buffer, 3);
    // convert bytes to value
    return BytesToUInt24(buffer);
}

u_int16_t 
ByteStream::readUInt16() 
{
    u_int8_t buffer[2];
    // TODO: Error handling
    read(buffer, 2);
    // convert bytes to value
    return BytesToUInt16(buffer);
}

u_int8_t 
ByteStream::readUInt8() 
{
    u_int8_t buffer[1];
    // TODO: Error handling
    read(buffer, 1);
    // convert bytes to value
    return buffer[0];
}

double 
ByteStream::readDouble() 
{
    u_int8_t buffer[8];
    // TODO: Error handling
    read(buffer, 8);
    // convert bytes to value
    return BytesToDouble(buffer);
}

void 
ByteStream::readString(char* buffer, int64_t size) 
{
    if (buffer == NULL || size == 0) {
        return;
    }

    int64_t bytesHasRead = 0;
    while (bytesHasRead < size - 1) {
        //TODO: Error handling
        read((u_int8_t*) &buffer[bytesHasRead], 1);
        if (buffer[bytesHasRead] == '\0') {
            // end of string
            return;
        }
        bytesHasRead++;
    }
    buffer[size - 1] = '\0';
}

const u_int8_t *ByteStream::getData() { return mData; }

int64_t ByteStream::getSize() const { return mSize; }

int64_t ByteStream::getOffset() const { return mOffset; }

void ByteStream::resetOffset(int64_t pos) { mOffset = pos; }

// Write functions
int64_t ByteStream::appendData(const u_int8_t* data, int64_t byteToWrite)
{
    if (data == NULL || byteToWrite == 0) {
        return 0;
    }

    int64_t newSize = byteToWrite + mOffset;

    if (newSize > mSize) {
        //Re-allocate memory
        u_int8_t* newData = new u_int8_t[newSize];

        // copy the contents of the previous buffer ,is any
        if (mData && mSize) {
            memcpy(newData, mData, mSize);
            delete[] mData;
        }
        // destroy the previous buffer
        mData = newData;
        mSize = newSize;
    }
    memcpy(mData + mOffset, data, byteToWrite);
    return byteToWrite;
}

//
int64_t ByteStream::read(u_int8_t* buffer, int64_t bytesToRead)
{
    if (bytesToRead == 0) {
        return 0;
    }

    if (mOffset + bytesToRead > mSize) {
        bytesToRead = mSize - mOffset;
    }
    // check for end of stream
    if (bytesToRead == 0) {
        return -1; //nothing can read
    }

    // read from the memory
    memcpy(buffer, mData + mOffset, bytesToRead);
    mOffset += bytesToRead;

    return bytesToRead;
}