/*
 *   Copyright TBD by Liangfeng, All rights reserved.
 */
#include <iostream>
#include "Mp4Crawler.h"

int main(int argc, char* argv[])
{
    if (argc < 2) {
        std::cout << "Usage: " << argv[0] << " <URL>" << std::endl;
        return -1;
    }

    //"http://distribution.bbb3d.renderfarming.net/video/mp4/bbb_sunflower_native_60fps_stereo_abl.mp4"

    char* url = argv[1];
    const char* filename = strrchr(url, '/') + 1;
    //
    //FILE* outfile = fopen("/tmp/extractfrommp4.mp3", "wb"); //test purpose

    Mp4Crawler crawler(url, stdout);
    //internal event loop here
    crawler.crawl();

    //fclose(outfile);

    return 0;
}
