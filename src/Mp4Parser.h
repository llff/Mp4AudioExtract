/**
 *   Copyright TBD by Liangfeng, All rights reserved.
 */
#pragma once
#include <iostream>
#include "ByteStream.h"
#include "Mp4Content.h"

class Mp4Parser {

public:
    //ctor
    Mp4Parser(): mParsing(true) {}
    //Parsing function
    Result ParseMp4(ByteStream& stream, Mp4Content* mp4);

    static Result GetTrackInfo(const TrakBox* trakBox, TrackInfo* trackInfo);
protected:
    static void SkipBox(ByteStream& stream, Mp4Box* box);
    //parse box  header (size+type)
    Result ParseMp4Box(ByteStream& stream, Mp4Box* box);
    //parse 'ftyp' box
    Result ParseFtypBox(ByteStream& stream, FtypBox* ftypBox);
    //parse 'moov' box
    Result ParseMoovBox(ByteStream& stream, MoovBox* moovBox);
    //parse 'mvhd' box
    Result ParseMvhd(ByteStream& stream, MvhdBox* mvhdBox, MoovBox* moovBox);
    //parse 'trak' box
    Result ParseTrakBox(ByteStream& stream, TrakBox* trakBox, MoovBox* moovBox);
    //parse 'tkhd'
    Result ParseTkhd(ByteStream& stream, TkhdBox* tkhd, TrakBox* trakBox);
    //'edts'
    Result ParseEdtsBox(ByteStream& stream, EdtsBox* edts, TrakBox* trakBox);
    //'elts'
    Result ParseElst(ByteStream& stream, ElstBox* edts, EdtsBox* edtsBox);
    
    //'mdia'
    Result ParseMdiaBox(ByteStream& stream, MdiaBox* edtsBox, TrakBox* trakBox);
    //'mdia'-->'mdhd'
    Result ParseMdhd(ByteStream& stream, MdhdBox* mdhd, MdiaBox* mdiaBox);
    //'mdia'-->'hdlr'
    Result ParseHdlr(ByteStream& stream, HdlrBox* mdhd, MdiaBox* mdiaBox);
    //'mdia'-->'minf'
    Result ParseMinfBox(ByteStream& stream, MinfBox* minfBox, MdiaBox* mdiaBox);

    //'mdia'-->'minf'-->'vmhd'
    Result ParseVmhd(ByteStream& stream, VmhdBox* vmhd, MinfBox* minfBox);
    //'mdia'-->'minf'-->'smhd'
    Result ParseSmhd(ByteStream& stream, SmhdBox* vmhd, MinfBox* minfBox);
    //'mdia'-->'minf'-->'hmhd'
    Result ParseHmhd(ByteStream& stream, HmhdBox* vmhd, MinfBox* minfBox);
    //'mdia'-->'minf'-->'nmhd'
    Result ParseNmhd(ByteStream& stream, NmhdBox* vmhd, MinfBox* minfBox);
    //'mdia'-->'minf'-->'dinf'
    Result ParseDinfBox(ByteStream& stream, DinfBox* dinfBox, MinfBox* minfBox);
    //'mdia'-->'minf'-->'dinf'-->'dref'
    Result ParseDref(ByteStream& stream, DrefBox* dref, DinfBox* dinfBox);

    ///sampel boxes parser
    //'mdia'-->'minf'-->'stbl'
    Result ParseStblBox(ByteStream& stream, StblBox* stblBox, MinfBox* minfBox); //parent

    //'mdia'-->'minf'-->'stbl'-->'stsd'
    Result ParseStsd(ByteStream& stream, StsdBox* stsd, StblBox* stblBox);
    Result ParseStsdAudio(ByteStream& stream, Mp4Box* parentBox, TrakBox* trakBox);

    //'mdia'-->'minf'-->'stbl'-->'stts'
    Result ParseStts(ByteStream& stream, SttsBox* stts, StblBox* stblBox);
    //'mdia'-->'minf'-->'stbl'-->'ctts'
    Result ParseCtts(ByteStream& stream, CttsBox* ctts, StblBox* stblBox);
    //'mdia'-->'minf'-->'stbl'-->'cslg'
    Result ParseCslg(ByteStream& stream, CslgBox* cslg, StblBox* stblBox);
    //'mdia'-->'minf'-->'stbl'-->'stsc'
    Result ParseStsc(ByteStream& stream, StscBox* stsc, StblBox* stblBox);
    //-----
    //'mdia'-->'minf'-->'stbl'-->'stsz'
    Result ParseStsz(ByteStream& stream, StszBox* stsz, StblBox* stblBox);
    //'mdia'-->'minf'-->'stbl'-->'stz2'
    Result ParseStz2(ByteStream& stream, Stz2Box* stz2, StblBox* stblBox);
    //'mdia'-->'minf'-->'stbl'-->'stco'
    Result ParseStco(ByteStream& stream, StcoBox* stco, StblBox* stblBox);
    //'mdia'-->'minf'-->'stbl'-->'co64'
    Result ParseCo64(ByteStream& stream, Co64Box* co64, StblBox* stblBox);
    //'mdia'-->'minf'-->'stbl'-->'stss'
    Result ParseStss(ByteStream& stream, StssBox* stss, StblBox* stblBox);
    //-----
    //'mdia'-->'minf'-->'stbl'-->'stsh'
    Result ParseStsh(ByteStream& stream, StshBox* stsh, StblBox* stblBox);
    //'mdia'-->'minf'-->'stbl'-->'padb'
    Result ParsePadb(ByteStream& stream, PadbBox* padb, StblBox* stblBox);
    //'mdia'-->'minf'-->'stbl'-->'stdp'
    Result ParseStdp(ByteStream& stream, StdpBox* stdp, StblBox* stblBox);
    //'mdia'-->'minf'-->'stbl'-->'sdtp'
    Result ParseSdtp(ByteStream& stream, SdtpBox* sdtp, StblBox* stblBox);
    //'mdia'-->'minf'-->'stbl'-->'sbgp'
    Result ParseSbgp(ByteStream& stream, SbgpBox* sbgp, StblBox* stblBox);
    //-----
    //'mdia'-->'minf'-->'stbl'-->'sgpd'
    Result ParseSgpd(ByteStream& stream, SgpdBox* sgpd, StblBox* stblBox);
    //'mdia'-->'minf'-->'stbl'-->'subs'
    Result ParseSubs(ByteStream& stream, SubsBox* subs, StblBox* stblBox);
    //'mdia'-->'minf'-->'stbl'-->'saiz'
    Result ParseSaiz(ByteStream& stream, SaizBox* saiz, StblBox* stblBox);
    //'mdia'-->'minf'-->'stbl'-->'saio'
    Result ParseSaio(ByteStream& stream, SaioBox* saio, StblBox* stblBox);

private:
    bool mParsing;
    Mp4Parser(const Mp4Parser&) = delete;
};