/**
 *   Copyright TBD by Liangfeng, All rights reserved.
 */
#include "Mp4Parser.h"
#include "Mp4Box.h"

Result Mp4Parser::ParseMp4(ByteStream& stream, Mp4Content* mp4Content)
{
    int64_t dataSize = stream.getOffset();

    stream.resetOffset(0);
    Result result = OK;
    //sub boxes
    while (mParsing && result == OK &&  stream.getOffset() <= stream.getDataSize() ) {
        Mp4Box mp4box;
        result = ParseMp4Box(stream, &mp4box);
        if(result != OK) {
            return result; //stop parsing
        }
        switch (mp4box.getType()) {
            case BOX_MOOV: {
                    MoovBox* moovBox = new MoovBox(mp4box.getSize());
                    moovBox->mEndPos = mp4box.mEndPos;
                    result = ParseMoovBox(stream, moovBox);
                    mp4Content->setMoovBox(moovBox);
                }
                break;
            case BOX_FTYP: {
                    FtypBox *ftypBox = new FtypBox(mp4box.getSize());
                    ftypBox->mEndPos = mp4box.mEndPos;
                    result = ParseFtypBox(stream, ftypBox);
                    mp4Content->setFtypBox(ftypBox);
                }
                break;
            case BOX_MDAT:
                //TODO:
            default:
                // Unknown box
                //std::cout << "Found Unknown box:" << mp4box.mType << std::endl;
                SkipBox(stream, &mp4box);
        }
    }

    return result;
}

Result Mp4Parser::GetTrackInfo(const TrakBox* trakBox, TrackInfo* trackInfo) 
{
    if (trakBox == NULL || trackInfo == NULL) {
        return ERROR;
    }
    trackInfo->mHandlerType = trakBox->mHandlerType;
    trackInfo->mCodec = trakBox->mCodec;
    trackInfo->mSampleCount = trakBox->mStszSampleCount;

    u_int32_t sampleSizeCbr = 0;
    if (!trakBox->mStszEntrySize) {
        sampleSizeCbr = trakBox->mStszSampleSize;
    }

    for (uint32_t i = 0; i < trackInfo->mSampleCount; i++) {
        trackInfo->mSampleType = new u_int32_t[trackInfo->mSampleCount];
        trackInfo->mSampleSize = new u_int32_t[trackInfo->mSampleCount];
        trackInfo->mSampleOffset = new int64_t[trackInfo->mSampleCount];
    }
    // Set sample type & size
    for (uint32_t i = 0; i < trackInfo->mSampleCount; i++) {
        unsigned sampleIndex = i;  // Sample id
        // Set sample type
        switch (trackInfo->mHandlerType) {
            case HANDLER_VIDEO:
                trackInfo->mSampleType[sampleIndex] = MEDIA_VIDEO;
                for (uint32_t j = 0; j < trakBox->mStssEntryCount; j++) {
                    if (i == (trakBox->mStssSampleNumber[j] - 1))
                        trackInfo->mSampleType[sampleIndex] = MEDIA_VIDEO_SYNC;
                }
                break;
            case HANDLER_AUDIO:
                trackInfo->mSampleType[sampleIndex] = MEDIA_AUDIO;
                break;
            default:  // Don't care
                trackInfo->mSampleType[sampleIndex] = MEDIA_UNKNOWN;
        }
        // Set sample size
        if (trakBox->mStszEntrySize) {
            trackInfo->mSampleSize[sampleIndex] = trakBox->mStszEntrySize[i];
        } else {
            trackInfo->mSampleSize[sampleIndex] = sampleSizeCbr;
        }
    }

    // Set sample offset
    u_int32_t index = 0;
    u_int32_t chunkOffset = 0;

    for (u_int32_t i = 0;
         (i < trakBox->mStscEntryCount) && (chunkOffset < trakBox->mStcoEntryCount); i++) {
        u_int32_t n = 0;
        if ((i + 1) == trakBox->mStscEntryCount) {
            if ((trakBox->mStscEntryCount > 1) && (chunkOffset == 0)) {
                n = 1;
            } else {
                n = trakBox->mStcoEntryCount - chunkOffset;
            }
        } else {
            n = trakBox->mStscFirstChunk[i + 1] - trakBox->mStscFirstChunk[i];
        }

        for (u_int32_t k = 0; k < n; k++) {
            for (u_int32_t l = 0; l < trakBox->mStscSamplesPerChunk[i]; l++) {
                if (l == 0) {
                    trackInfo->mSampleOffset[index++] = trakBox->mStcoChunkOffset[chunkOffset];
                } else {
                    trackInfo->mSampleOffset[index] = trackInfo->mSampleOffset[index - 1] +
                                                      (int64_t)(trackInfo->mSampleSize[index - 1]);
                    index++;
                }
            }
            chunkOffset++;  // Increase chunk offset
        }
    }

    return OK;
}
Result Mp4Parser::ParseMp4Box(ByteStream& stream, Mp4Box* box)
{
    if (box == nullptr) {
        mParsing = false;
        return ERROR;
    } 
    int64_t pos = stream.getOffset();
    box->mSize = stream.readUInt32();
    box->mType = stream.readUInt32();

    bool largeSize = false;
    if (box->getSize() == 0) {
        //to end of file
        int64_t dataSize = stream.getSize();
        if (dataSize >= stream.getOffset()) {
            box->mSize = dataSize - stream.getOffset();
        }
    } else if (box->getSize() == 1) {
        // 64-bit size
        box->mIsLargeSize = true;
        box->mSize = stream.readUInt64();
    }
    /*
    if (boxtype==�uuid�) {
        unsigned int(8)[16] usertype = extended_type;
    }
    */
    if (box->mType == BOX_UUID) {
        //
        u_int8_t extendType[16]; //TODO: Need save?
        for (int i = 0; i < 16; i++)
            extendType[i] = stream.readUInt8();
    }

    // check the size
    if ((box->mSize > 0 && box->mSize < 8)) {
        stream.resetOffset(pos);
        mParsing = false;
        return ERROR; //Something wrong
    }

    box->mEndPos = pos + box->mSize;

    return OK;
}

Result Mp4Parser::ParseFtypBox(ByteStream& stream, FtypBox* ftypBox)
{
    u_int32_t majorBrand = stream.readUInt32();

    if (majorBrand == FourCCToUInt32("qt  "))
        ftypBox->mProfile = PROF_ISOBMF_MOV;
    else if (majorBrand == FourCCToUInt32("3gp4"))
        ftypBox->mProfile = PROF_ISOBMF_3GP;

    // Read informative integer for the minor version of the major brand
    ftypBox->mMinorVersion = stream.readUInt32();

    // Read a list of brands, until the end of the box
    //u_int32_t compatible_brands[16] = {0};

    u_int32_t i = 0;
    u_int32_t numOfBrands = static_cast<u_int32_t> (ftypBox->getSize() - 16) / 4;

    if (numOfBrands > 16)
        numOfBrands = 16;
    for (i = 0; i < numOfBrands; i++) {
        ftypBox->mCompatiblBrands[i] = stream.readUInt32();
        if (ftypBox->mProfile == 0) {
            if (ftypBox->mCompatiblBrands[i] == FourCCToUInt32("mp41") ||
                ftypBox->mCompatiblBrands[i] == FourCCToUInt32("mp42")) {
                ftypBox->mProfile = PROF_ISOBMF_MP4;
            } else if (ftypBox->mCompatiblBrands[i] == FourCCToUInt32("3gp4")) {
                ftypBox->mProfile = PROF_ISOBMF_3GP;
            }
        }
    }
    return OK;
}

Result Mp4Parser::ParseMoovBox(ByteStream& stream, MoovBox* moovBox)
{
    Result result = OK;
//
    while (mParsing && result == OK && stream.getOffset() < (moovBox->mEndPos - 8) ) {
        Mp4Box mp4box;
        result = ParseMp4Box(stream, &mp4box);
        if (result != OK) {
            return result; // stop parsing
        }
        switch (mp4box.mType) {
            case BOX_MVHD: {
                MvhdBox *mvhdBox = new MvhdBox(mp4box.mSize, moovBox);
                mvhdBox->mEndPos = mp4box.mEndPos;
                result = ParseMvhd(stream, mvhdBox, moovBox);
            }
            break;
            case BOX_TRAK: {
                TrakBox* trakBox = new TrakBox(mp4box.mSize, moovBox);
                trakBox->mEndPos = mp4box.mEndPos;
                result = ParseTrakBox(stream, trakBox, moovBox);
            }
            break;
            case BOX_IODS:
            case BOX_META:
            case BOX_UDTA:
            default:
                // Unknown box
                //std::cout << "Found Unknown box:" << mp4box.mType << std::endl;
                SkipBox(stream, &mp4box);
        }
    }
    return result;
}

Result Mp4Parser::ParseMvhd(ByteStream &stream, MvhdBox *mvhdBox,
                            MoovBox *moovBox) {
    // connect to parent
    moovBox->mMvhd = mvhdBox;

    mvhdBox->mVersion = stream.readUInt8();
    mvhdBox->mFlags = stream.readUInt24();

    // Read box content
    if (mvhdBox->mVersion == 1) {
        mvhdBox->mCreationTime = stream.readUInt64();
        mvhdBox->mModificationTime = stream.readUInt64();
        mvhdBox->mTimescale = stream.readUInt32();
        mvhdBox->mDuration = stream.readUInt64();
    } else { // if (mvhdBox->mVersion == 0)
        mvhdBox->mCreationTime = stream.readUInt32();
        mvhdBox->mModificationTime = stream.readUInt32();
        mvhdBox->mTimescale = stream.readUInt32();
        mvhdBox->mDuration = stream.readUInt32();
    }
    // template int(32) rate = 0x00010000; // typically 1.0
    // template int(16) volume = 0x0100;   // typically, full volume
    u_int32_t rate = stream.readUInt32();
    u_int32_t volume = stream.readUInt16();
    stream.readUInt16(); // Reserved = 0
    stream.readUInt32(); // Reserved = 0
    stream.readUInt32(); // Reserved = 0

    // Provides a transformation matrix for the video;
    int32_t matrix[9];
    for (int i = 0; i < 9; i++) {
        matrix[i] = stream.readUInt32();
    }

    // Unity matrix
    int32_t predefined[6];
    for (int i = 0; i < 6; i++) {
        predefined[i] = stream.readUInt32();
    }

    mvhdBox->mNextTrackId = stream.readUInt32();

    return OK;
}

Result Mp4Parser::ParseTrakBox(ByteStream& stream, TrakBox* trakBox, MoovBox* moovBox) 
{
    Result result = OK;

    Mp4Box mp4box;
    while (mParsing && result == OK && stream.getOffset() < (trakBox->mEndPos - 8)) {
        Mp4Box mp4box;
        result = ParseMp4Box(stream, &mp4box);
        if (result != OK) {
            return result; // stop parsing
        }
        switch (mp4box.mType) {
            case BOX_TKHD: {
                TkhdBox* tkhd = new TkhdBox(mp4box.mSize, trakBox);
                tkhd->mEndPos = mp4box.mEndPos;
                result = ParseTkhd(stream, tkhd, trakBox);
            }
            break;
            case BOX_EDTS: {
                EdtsBox* edtsBox = new EdtsBox(mp4box.mSize, trakBox);
                edtsBox->mEndPos = mp4box.mEndPos;
                result = ParseEdtsBox(stream, edtsBox, trakBox);
            }
            break;
            case BOX_MDIA: {
                MdiaBox* mdiaBox = new MdiaBox(mp4box.mSize, trakBox);
                mdiaBox->mEndPos = mp4box.mEndPos;
                result = ParseMdiaBox(stream, mdiaBox, trakBox);
            }
            break;
            case BOX_UDTA:
            case BOX_UUID:
            default: {
                // Unknown box
                //std::cout<<"Found Unknown box: " <<mp4box.mType <<std::endl;
                SkipBox(stream, &mp4box);
            }
            break;
        }
    }
    //connect to parent
    moovBox->addTrack(trakBox);
    return result;
}

Result Mp4Parser::ParseTkhd(ByteStream& stream, TkhdBox* tkhd, TrakBox* trakBox) 
{
    // connect to parent
    trakBox->mTkhd = tkhd;

    // Read FullBox
    tkhd->mVersion = stream.readUInt8();
    tkhd->mFlags = stream.readUInt24();

    // Read box content
    if (tkhd->mVersion == 1) {
        tkhd->mCreationTime = stream.readUInt64();
        tkhd->mModificationTime = stream.readUInt64();
        tkhd->mId = stream.readUInt32();
        tkhd->mReserved = stream.readUInt32();
        tkhd->mDuration = stream.readUInt64();
    } else { // if (tkhd->mVersion == 0)
        tkhd->mCreationTime = stream.readUInt32();
        tkhd->mModificationTime = stream.readUInt32();
        tkhd->mId = stream.readUInt32();
        tkhd->mReserved = stream.readUInt32();
        tkhd->mDuration = stream.readUInt32();
    }

    u_int32_t reserved[2] = {0};
    reserved[0] = stream.readUInt32();
    reserved[1] = stream.readUInt32();

    //Skip
    u_int16_t layer = stream.readUInt16();
    u_int16_t alternate_group = stream.readUInt16();
    u_int16_t volume = stream.readUInt16();
    u_int16_t reserved2 = stream.readUInt16();

    u_int32_t matrix[9] = {0};
    for (int i = 0; i < 9; i++) {
        matrix[i] = stream.readUInt32();
    }
    //

    tkhd->mWidth = stream.readUInt32();
    tkhd->mHeight = stream.readUInt32();

    return OK;

}

Result Mp4Parser::ParseEdtsBox(ByteStream& stream, EdtsBox* edtsBox, TrakBox* trakBox)
{
    // connect to parent
    trakBox->mEdtsBox = edtsBox;
    Result result = OK;
    
    while (mParsing && result == OK && stream.getOffset() < (edtsBox->mEndPos - 8)) {
        Mp4Box mp4box;
        result = ParseMp4Box(stream, &mp4box);
        if (result != OK) {
            return result;  // stop parsing
        }
        switch (mp4box.mType) {
            case BOX_ELST: {
                ElstBox* elts = new ElstBox(mp4box.mSize, edtsBox);
                elts->mEndPos = mp4box.mEndPos;
                result = ParseElst(stream, elts, edtsBox);
            } 
            break;
            default: {
                //Unknown box
                //std::cout<<"Found Unknown box"<<std::endl;
                SkipBox(stream, &mp4box);
            }
        }
    }
    return result;
}

Result Mp4Parser::ParseElst(ByteStream& stream, ElstBox* elst, EdtsBox* edtsBox) 
{  
    //Connect to parent
    edtsBox->mElst = elst;
    // Read FullBox
    elst->mVersion = stream.readUInt8();
    elst->mFlags = stream.readUInt24();

    u_int32_t count = stream.readUInt32();
    // Read box content
    if (elst->mVersion == 1) {
        for (u_int32_t i = 0; i < count; i++) {
            elst->mSegmentDuration = stream.readUInt64();
            elst->mMediaTime = stream.readUInt64();
            elst->mMediaRateInterger = stream.readUInt16();
            elst->mMediaRateFraction = stream.readUInt16();
        }
        
    } else { // if (elst->mVersion == 0)
        for (u_int32_t i = 0; i < count; i++) {
            elst->mSegmentDuration = stream.readUInt32();
            elst->mMediaTime = stream.readUInt32();
            elst->mMediaRateInterger = stream.readUInt16();
            elst->mMediaRateFraction = stream.readUInt16();
        }
    }
    return OK;

}

Result Mp4Parser::ParseMdiaBox(ByteStream& stream, MdiaBox* mdiaBox, TrakBox* trakBox)
{
    // connect to parent
    trakBox->mMdiaBox = mdiaBox;
    Result result = OK;
    //sub boxes
    while (mParsing && result == OK && stream.getOffset() < (mdiaBox->mEndPos - 8)) {
        Mp4Box mp4box;
        result = ParseMp4Box(stream, &mp4box);
        if (result != OK) {
            return result;  // stop parsing
        }
        switch (mp4box.mType) {
            case BOX_MDHD: {
                MdhdBox* mdhd = new MdhdBox(mp4box.mSize, mdiaBox);
                mdhd->mEndPos = mp4box.mEndPos;
                result = ParseMdhd(stream, mdhd, mdiaBox);
            }
            break;
            case BOX_HDLR: {
                HdlrBox* hdlr = new HdlrBox(mp4box.mSize, mdiaBox);
                hdlr->mEndPos = mp4box.mEndPos;
                result = ParseHdlr(stream, hdlr, mdiaBox);
            }
            break;
            case BOX_MINF: {
                MinfBox* minfBox = new MinfBox(mp4box.mSize, mdiaBox);
                minfBox->mEndPos = mp4box.mEndPos;
                result = ParseMinfBox(stream, minfBox, mdiaBox);
            }
            break;
            default: {
                // Unknown box
                //std::cout << "Found Unknown box" << std::endl;
                SkipBox(stream, &mp4box);
            }
        }
    }
    return result;

}

Result Mp4Parser::ParseMdhd(ByteStream& stream, MdhdBox* mdhd, MdiaBox* mdiaBox)
{
    // connect to parent
    mdiaBox->mMdhd = mdhd;
    // Read FullBox
    mdhd->mVersion = stream.readUInt8();
    mdhd->mFlags = stream.readUInt24();


    // Read box content
    if (mdhd->mVersion == 1) {
        mdhd->mCreateTime = stream.readUInt64();
        mdhd->mModifictioniTime = stream.readUInt64();
        mdhd->mTimescale = stream.readUInt32();
        mdhd->mDuration = stream.readUInt64();
    } else  { // mVersion == 0
        mdhd->mCreateTime = stream.readUInt32();
        mdhd->mModifictioniTime = stream.readUInt32();
        mdhd->mTimescale = stream.readUInt32();
        mdhd->mDuration = stream.readUInt32();
    }

    
    u_int16_t lang = stream.readUInt16();  //first bit is pad 
    mdhd->mLanguage[0] = (u_int8_t)(lang & 0x7C00) + 96; // bit 1~5
    mdhd->mLanguage[1] = (u_int8_t)(lang & 0x03E0) + 96; // bit 6~10
    mdhd->mLanguage[2] = (u_int8_t)(lang & 0x001F) + 96; // bit 11-15

    // u_int16_t pre_defined
    stream.readUInt16();

    return OK;
}

Result Mp4Parser::ParseHdlr(ByteStream& stream, HdlrBox* hdlr, MdiaBox* mdiaBox)
{
    // connect to parent
    mdiaBox->mHdlr = hdlr;
    // Read FullBox
    hdlr->mVersion = stream.readUInt8();
    hdlr->mFlags = stream.readUInt24();
  
    // Read box content
    u_int32_t preDefined = stream.readUInt32();
    u_int32_t handlerType = stream.readUInt32();
    
    //trak->mdia->hdlr
    char* trkName = nullptr;
    if (mdiaBox && mdiaBox->mParent && mdiaBox->mParent->mHandlerType == 0) {
        mdiaBox->mParent->mHandlerType = handlerType;
        trkName = mdiaBox->mParent->mTrackName; //256 bytes
    }

    u_int32_t reserved[3];
    reserved[0] = stream.readUInt32();
    reserved[1] = stream.readUInt32();
    reserved[2] = stream.readUInt32();

    int64_t bytesLeft = hdlr->mSize - 32;
    if (bytesLeft > 0) {
        u_int8_t namesize = *(u_int8_t*)(stream.getData() + stream.getOffset());
        if (bytesLeft == namesize + 1) {
            bytesLeft = stream.readUInt8(); //need? already read
        }
        if (bytesLeft > 128)
            bytesLeft = 128;
        for (int i = 0; i < bytesLeft; i++) {
            if (trkName) {
                trkName[i] = stream.readUInt8();
            }
        }  
    }
    return OK;
}

Result Mp4Parser::ParseMinfBox(ByteStream& stream, MinfBox* minfBox, MdiaBox* mdiaBox)
{
    Result result = OK;
    // connect to parent
    mdiaBox->mMinf = minfBox;
    //sub boxes
    while (mParsing && result == OK && stream.getOffset() < (minfBox->mEndPos - 8)) {
        Mp4Box mp4box;
        result = ParseMp4Box(stream, &mp4box);
        if (result != OK) {
            return result;  // stop parsing
        }
        switch (mp4box.mType) {
            case BOX_VMHD: {
                VmhdBox* vmhd = new VmhdBox(mp4box.mSize, minfBox);
                vmhd->mEndPos = mp4box.mEndPos;
                result = ParseVmhd(stream, vmhd, minfBox);
            } break;
            case BOX_SMHD: {
                SmhdBox* sdhd = new SmhdBox(mp4box.mSize, minfBox);
                sdhd->mEndPos = mp4box.mEndPos;
                result = ParseSmhd(stream, sdhd, minfBox);
            } break;
            case BOX_HMHD: {
                HmhdBox* hmhd = new HmhdBox(mp4box.mSize, minfBox);
                hmhd->mEndPos = mp4box.mEndPos;
                result = ParseHmhd(stream, hmhd, minfBox);
            } break;
            case BOX_NMHD: {
                NmhdBox* nmhd = new NmhdBox(mp4box.mSize, minfBox);
                nmhd->mEndPos = mp4box.mEndPos;
                result = ParseNmhd(stream, nmhd, minfBox);
            } break;
            case BOX_DINF: {
                DinfBox* dinfBox = new DinfBox(mp4box.mSize, minfBox);
                dinfBox->mEndPos = mp4box.mEndPos;
                result = ParseDinfBox(stream, dinfBox, minfBox);
            } break;
            case BOX_STBL: {
                StblBox* stblBox = new StblBox(mp4box.mSize, minfBox);
                stblBox->mEndPos = mp4box.mEndPos;
                result = ParseStblBox(stream, stblBox, minfBox);
            } break;

            default: {
                // Unknown box
                //std::cout << "Found Unknown box" << std::endl;
                SkipBox(stream, &mp4box);
            }
        }
    }
    return result;
}

Result Mp4Parser::ParseVmhd(ByteStream& stream, VmhdBox* vmhd, MinfBox* minfBox)
{
    Result result = OK;
    // connect to parent
    minfBox->mVmhd = vmhd;
    // Read FullBox
    vmhd->mVersion = stream.readUInt8();
    vmhd->mFlags = stream.readUInt24();

    // Read box content
    vmhd->mGraphicsmode = stream.readUInt16();
    u_int16_t opcolor[3] = {0};
    for (int i = 0; i < 3; i++) {
        vmhd->mOpcolor[i] = stream.readUInt16();
    }
    return result;
}
//'smhd'
Result Mp4Parser::ParseSmhd(ByteStream& stream, SmhdBox* smhd, MinfBox* minfBox)
{
    Result result = OK;
    // connect to parent
    minfBox->mSmhd = smhd;
    // Read FullBox attributes
    smhd->mVersion = stream.readUInt8();
    smhd->mFlags = stream.readUInt24();

    // Read box content
    smhd->mBalance = (int16_t) stream.readUInt16();
    //u_int16_t reserved
    stream.readUInt16();
    return result;
}
//'hmhd'
Result Mp4Parser::ParseHmhd(ByteStream& stream, HmhdBox* hmhd, MinfBox* minfBox)
{
    Result result = OK;
    // connect to parent
    minfBox->mHmhd = hmhd;
    // Read FullBox attributes
    hmhd->mVersion = stream.readUInt8();
    hmhd->mFlags = stream.readUInt24();

    hmhd->mMaxPduSize = stream.readUInt16();
    hmhd->mAvgPduSize = stream.readUInt16();
    hmhd->mMaxBitrate = stream.readUInt32();
    hmhd->mAvgBitrate = stream.readUInt32();

    // u_int32_t reserved
    stream.readUInt32();

    return result;
}
//'nmhd'
Result Mp4Parser::ParseNmhd(ByteStream& stream, NmhdBox* nmhd, MinfBox* minfBox)
{
    Result result = OK;
    // connect to parent
    minfBox->mNmhd = nmhd;
    // Read FullBox attributes
    nmhd->mVersion = stream.readUInt8();
    nmhd->mFlags = stream.readUInt24();
    return result;
}
//
Result Mp4Parser::ParseDinfBox(ByteStream& stream, DinfBox* dinfBox, MinfBox* minfBox)
{
    Result result = OK;
    // connect to parent
    minfBox->mDinf = dinfBox;

    //sub boxes
    while (mParsing && result == OK && stream.getOffset() < (dinfBox->mEndPos - 8)) {
        Mp4Box mp4box;
        result = ParseMp4Box(stream, &mp4box);
        if (result != OK) {
            return result;  // stop parsing
        }
        switch (mp4box.mType) {
            case BOX_DREF: {
                DrefBox* elts = new DrefBox(mp4box.mSize, dinfBox);
                result = ParseDref(stream, elts, dinfBox);
            } break;
            default: {
                // Unknown box
                //std::cout << "Found Unknown box" << std::endl;
                SkipBox(stream, &mp4box);
            }
        }
    }
    return result;
}

//'dref'
Result Mp4Parser::ParseDref(ByteStream& stream, DrefBox* dref, DinfBox* dinfBox)
{
    Result result = OK;
    // connect to parent
    dinfBox->mDref = dref;
    // Read FullBox attributes
    dref->mVersion = stream.readUInt8();
    dref->mFlags = stream.readUInt24();
    dref->mEntryCount = stream.readUInt32();
    
    for (u_int32_t i = 0; i < dref->mEntryCount && result == OK; ++i)
    {
        Mp4Box mp4box;
        result = ParseMp4Box(stream, &mp4box);
        if (result != OK) {
            return result;  // stop parsing
        }
        switch (mp4box.mType) {
            case BOX_URL:
            case BOX_URN: {
                stream.readUInt8();
                stream.readUInt24();
        }
        break;
        default: {
            // Unknown box
            //std::cout << "Found Unknown box" << std::endl;
            SkipBox(stream, &mp4box);
        }
        }
    }
    return result;
}


Result Mp4Parser::ParseStblBox(ByteStream& stream, StblBox* stblBox, MinfBox* minfBox)
{
    Result result = OK;
    // connect to parent
    minfBox->mStblBox = stblBox;

    //sub boxes
    while (mParsing && result == OK && stream.getOffset() < (stblBox->mEndPos - 8)) {
        Mp4Box mp4box;
        result = ParseMp4Box(stream, &mp4box);
        if (result != OK) {
            return result;  // stop parsing
        }
        switch (mp4box.mType) {
            case BOX_STSD: {
                StsdBox* stsd = new StsdBox(mp4box.mSize, stblBox);
                result = ParseStsd(stream, stsd, stblBox);
            } break;
            case BOX_STTS: {
                SttsBox* stts = new SttsBox(mp4box.mSize, stblBox);
                result = ParseStts(stream, stts, stblBox);
            } break;
            case BOX_CTTS: {
                CttsBox* ctts = new CttsBox(mp4box.mSize, stblBox);
                result = ParseCtts(stream, ctts, stblBox);
            } break;
            case BOX_STSS: {
                StssBox* stss = new StssBox(mp4box.mSize, stblBox);
                result = ParseStss(stream, stss, stblBox);
            } break;
            case BOX_STSC: {
                StscBox* stsc = new StscBox(mp4box.mSize, stblBox);
                result = ParseStsc(stream, stsc, stblBox);
            } break;
            case BOX_STSZ: {
                StszBox* stsz = new StszBox(mp4box.mSize, stblBox);
                result = ParseStsz(stream, stsz, stblBox);
            } break;
            case BOX_STZ2: {
                Stz2Box* stz2 = new Stz2Box(mp4box.mSize, stblBox);
                result = ParseStz2(stream, stz2, stblBox);
            } break;
            case BOX_STCO: {
                StcoBox* stco = new StcoBox(mp4box.mSize, stblBox, BOX_STCO);
                result = ParseStco(stream, stco, stblBox);
            } break;
            case BOX_CO64: {
                StcoBox* stco = new StcoBox(mp4box.mSize, stblBox, BOX_CO64);
                result = ParseStco(stream, stco, stblBox);
            } break;
            //case BOX_SDTP: {
            //    StdpBox* sdtp = new StdpBox(mp4box.mSize, stblBox);
            //    result = ParseStdp(stream, sdtp, stblBox);
            //} break;
            default: {
                // Unknown box
                //std::cout << "Found Unknown box" << std::endl;
                SkipBox(stream, &mp4box);
                //result = ERROR;
            }
        }
    }
    return result;
}

void Mp4Parser::SkipBox(ByteStream& stream, Mp4Box* box) 
{
    int64_t hdrSize = box->mIsLargeSize ? 16: 8;
    if (box->mType == BOX_UUID) {
        hdrSize += 16;
    }
    stream.resetOffset(stream.getOffset() + box->getSize() - hdrSize);
}

///sample boxes parser
/*
aligned(8) class SampleDescriptionBox (unsigned int(32) handler_type)
extends FullBox('stsd', 0, 0) {
    int i ;
    unsigned int(32) entry_count;
    for (i = 1 ; i <= entry_count ; i++){
        switch (handler_type) {
        case �soun�: // for audio tracks
            AudioSampleEntry();
            break;
        case �vide�: // for video tracks
            VisualSampleEntry();
            break;
        case �hint�: // Hint track
            HintSampleEntry();
            break;
        case �meta�: // Metadata track
            MetadataSampleEntry();
            break;
        }
    }
}
*/
Result Mp4Parser::ParseStsd(ByteStream& stream, StsdBox* stsd, StblBox* stblBox)
{
    Result result = OK;
    // connect to parent
    stblBox->mStsd = stsd;
    // Read FullBox attributes
    stsd->mVersion = stream.readUInt8();
    stsd->mFlags = stream.readUInt24();

    //TODO: stbl->minf->mida-trak
    TrakBox* trakBox = stblBox->mParent->mParent->mParent;
    u_int16_t handlerType = stblBox->mParent->mParent->mParent->mHandlerType;

    u_int32_t entryCount = stream.readUInt32();
    for (u_int32_t i = 0; i < entryCount; ++i) {
        Mp4Box mp4box;
        result = ParseMp4Box(stream, &mp4box);
        if (result != OK) {
            return result;  // stop parsing
        }
        if (handlerType != HANDLER_AUDIO) {
            //Skip other tracks
            SkipBox(stream, &mp4box);
            continue;
        }
        //track->fcc = box_subheader.boxtype;  // save fourcc as backup

        //SampleEntry
        /*
        aligned(8) abstract class SampleEntry (unsigned int(32) format)
            extends Box(format){
                const unsigned int(8)[6] reserved = 0;
                unsigned int(16) data_reference_index;
            }
        */

        switch (handlerType) {
            case HANDLER_AUDIO: {
                u_int8_t reserved[6] = {0};
                for (int j = 0; j < 6; j++) {
                    reserved[j] = stream.readUInt8();
                }
                // u_int16_t refIndex =
                stream.readUInt16();
                // 8 bytes skipped
                ParseStsdAudio(stream, &mp4box, trakBox);
            } break;
            case HANDLER_VIDEO:
            case HANDLER_TMCD:
            case HANDLER_TEXT:
            case HANDLER_META:
            case HANDLER_HINT:
            default: {
                // Unknown box
                //std::cout << "Found Unknown box" << std::endl;
                SkipBox(stream, &mp4box);
            }
        }
    }

    return result;
}

Result Mp4Parser::ParseStsdAudio(ByteStream& stream, Mp4Box* parentBox, TrakBox* trakBox)
{
    Result result = OK;
    char fcc[5];

    // AudioSampleEntry
    /*
    class AudioSampleEntry(codingname) extends SampleEntry(codingname)
    {
        const unsigned int(32)[2] reserved = 0;
        template unsigned int(16) channelcount = 2;
        template unsigned int(16) samplesize = 16;
        unsigned int(16) pre_defined = 0;
        const unsigned int(16) reserved = 0;
        template unsigned int(32) samplerate = {default samplerate of media} << 16;
    }
    */
    trakBox->mCodec = CODEC_UNKNOWN;
    if (parentBox->getType() == FourCCToUInt32("AC-3") || FourCCToUInt32("ac-3")) {
        trakBox->mCodec = CODEC_AC3;
    } else if (parentBox->getType() == FourCCToUInt32("AC-4") || FourCCToUInt32("ac-4")) {
        trakBox->mCodec = CODEC_AC4;
    } else if (parentBox->getType() == FourCCToUInt32("sowt")) {
        trakBox->mCodec = CODEC_LPCM;
    } 
    //else if (type == BOX_ESDS) {
    //    trakBox->mCodec = CODEC_UNKNOWN;
    //}

    //unsigned int(32)[2] reserved = 0;
    stream.readUInt32();
    stream.readUInt32();

    trakBox->mChannelCnt = stream.readUInt16();//is the number of channels such as 1 (mono) or 2 (stereo)
    trakBox->mSampleSizeInBit = stream.readUInt16();//SampleSize in bits, and takes the default value of 16
     
    u_int16_t pre_defined = stream.readUInt16();
    u_int16_t reserved    = stream.readUInt16();
    trakBox->mSamplerate = stream.readUInt16();
    u_int16_t reserved4 = stream.readUInt16();    

    while (mParsing && result == OK && stream.getOffset() < (parentBox->mEndPos - 8)) {
        Mp4Box mp4box;
        result = ParseMp4Box(stream, &mp4box);
        if (result != OK) {
            return result;  // stop parsing
        }
        switch (mp4box.mType) {
            default:
                SkipBox(stream, &mp4box);
        }
    }

    return result;
}
/*
aligned(8) class TimeToSampleBox
extends FullBox(�stts�, version = 0, 0) {
unsigned int(32) entry_count;
int i;
for (i=0; i < entry_count; i++) {
unsigned int(32) sample_count;
unsigned int(32) sample_delta;
}
}
*/
Result Mp4Parser::ParseStts(ByteStream& stream, SttsBox* stts, StblBox* stblBox)
{
    Result result = OK;
    // connect to parent
    stblBox->mStts = stts;

    // Read FullBox attributes
    stts->mVersion = stream.readUInt8();
    stts->mFlags = stream.readUInt24();
    stts->mEntryCount = stream.readUInt32();

    TrakBox* trakBox = stblBox->mParent->mParent->mParent;
    trakBox->mSttsEntryCount = stts->mEntryCount;

    if (stts->mEntryCount > 0) {
        trakBox->mSttsSampleCount = new u_int32_t[stts->mEntryCount];
        trakBox->mSttsSampleDelta = new u_int32_t[stts->mEntryCount];
        for (u_int32_t i = 0; i < stts->mEntryCount; ++i) {
            trakBox->mSttsSampleCount[i] = stream.readUInt32();
            trakBox->mSttsSampleDelta[i] = stream.readUInt32();
        }
    }
    return result;
}

//'mdia'-->'minf'-->'stbl'-->'ctts'
Result Mp4Parser::ParseCtts(ByteStream& stream, CttsBox* ctts, StblBox* stblBox)
{
    Result result = OK;
    // connect to parent
    stblBox->mCtts= ctts;
    // Read FullBox attributes
    ctts->mVersion = stream.readUInt8();
    ctts->mFlags = stream.readUInt24();
    ctts->mEntryCount = stream.readUInt32();
    TrakBox* trakBox = stblBox->mParent->mParent->mParent;
    trakBox->mCttsEntryCount = ctts->mEntryCount;

    if (ctts->mEntryCount > 0) {
        trakBox->mCttsSampleCount = new u_int32_t[ctts->mEntryCount];
        trakBox->mCttsSampleOffset = new int64_t[ctts->mEntryCount];

        for (u_int32_t i = 0; i < ctts->mEntryCount; i++) {
            trakBox->mCttsSampleCount[i] = stream.readUInt32();
            if (ctts->mVersion == 0)
                trakBox->mCttsSampleOffset[i] = (int64_t)stream.readUInt32(); //unsigend
            else if (ctts->mVersion == 1)
                trakBox->mCttsSampleOffset[i] = (int64_t)stream.readUInt32(); //signed
        }
  
    }
    return result;
}

//'mdia'-->'minf'-->'stbl'-->'cslg'
Result Mp4Parser::ParseCslg(ByteStream& stream, CslgBox* cslg, StblBox* stblBox)
{
    Result result = OK;
    // connect to parent
    stblBox->mCslg = cslg;
    // Read FullBox attributes
    cslg->mVersion = stream.readUInt8();
    cslg->mFlags = stream.readUInt24();

    cslg->mCompositionToDTSShift = stream.readUInt32();
    cslg->mLeastDecodeToDisplayDelta = stream.readUInt32();
    cslg->mGreatestDecodeToDisplayDelta = stream.readUInt32();
    cslg->mCompositionStartTime = stream.readUInt32();
    cslg->mCompositionEndTime = stream.readUInt32();

    return result;
}
//'mdia'-->'minf'-->'stbl'-->'stss' sync (key, I-frame) sample map
/*
aligned(8) class SyncSampleBox
extends FullBox(�stss�, version = 0, 0) {
unsigned int(32) entry_count;
int i;
for (i=0; i < entry_count; i++) {
unsigned int(32) sample_number;
}
}
*/
Result Mp4Parser::ParseStss(ByteStream& stream, StssBox* stss, StblBox* stblBox)
{
    Result result = OK;
    // connect to parent
    stblBox->mStss = stss;
    // Read FullBox attributes
    stss->mVersion = stream.readUInt8();
    stss->mFlags = stream.readUInt24();
    stss->mEntryCount = stream.readUInt32();
    
    TrakBox* trakBox = stblBox->mParent->mParent->mParent;
    trakBox->mStssEntryCount = stss->mEntryCount;
    
    if (stss->mEntryCount > 0) {
        trakBox->mStssSampleNumber = new u_int32_t[stss->mEntryCount];
        for (u_int32_t i = 0; i <stss->mEntryCount; ++i) {
            trakBox->mStssSampleNumber[i] = stream.readUInt32();
        }
    }
    return result;
}
//'mdia'-->'minf'-->'stbl'-->'stsc' sample-to-chunk
Result Mp4Parser::ParseStsc(ByteStream& stream, StscBox* stsc, StblBox* stblBox)
{
    Result result = OK;
    // connect to parent
    stblBox->mStsc = stsc;
    // Read FullBox attributes
    stsc->mVersion = stream.readUInt8();
    stsc->mFlags = stream.readUInt24();
    stsc->mEntryCount = stream.readUInt32();

    TrakBox* trakBox = stblBox->mParent->mParent->mParent;
    trakBox->mStscEntryCount = stsc->mEntryCount;

    if (stsc->mEntryCount > 0) {
        trakBox->mStscFirstChunk = new u_int32_t[stsc->mEntryCount];
        trakBox->mStscSamplesPerChunk = new u_int32_t[stsc->mEntryCount];
        trakBox->mStscSampleDescriptionIndex = new u_int32_t[stsc->mEntryCount];

        for (u_int32_t i = 0; i < stsc->mEntryCount; i++) {
            trakBox->mStscFirstChunk[i] = stream.readUInt32();
            trakBox->mStscSamplesPerChunk[i] = stream.readUInt32();
            trakBox->mStscSampleDescriptionIndex[i] = stream.readUInt32();
        }
    }
    return result;
}
//-----
//'mdia'-->'minf'-->'stbl'-->'stsz' sample sizes (framing)
Result Mp4Parser::ParseStsz(ByteStream& stream, StszBox* stsz, StblBox* stblBox)
{
    Result result = OK;
    // connect to parent
    stblBox->mStsz= stsz;
    // Read FullBox attributes
    stsz->mVersion = stream.readUInt8();
    stsz->mFlags = stream.readUInt24();
    TrakBox* trakBox = stblBox->mParent->mParent->mParent;
    trakBox->mStszSampleSize = stream.readUInt32();
    trakBox->mStszSampleCount = stream.readUInt32();
   
    if (trakBox->mStszSampleSize == 0) {
        trakBox->mStszEntrySize = new u_int32_t[trakBox->mStszSampleCount];
        for (unsigned i = 0; i < trakBox->mStszSampleCount; i++) {
            trakBox->mStszEntrySize[i] = stream.readUInt32();
        }
    }
    return result;
}
//'mdia'-->'minf'-->'stbl'-->'stz2' compact sample sizes (framing)
/*
aligned(8) class CompactSampleSizeBox extends FullBox(�stz2�, version = 0, 0) {
unsigned int(24) reserved = 0;
unisgned int(8) field_size;
unsigned int(32) sample_count;
for (i=1; i <= sample_count; i++) {
unsigned int(field_size) entry_size;
}
}
*/
Result Mp4Parser::ParseStz2(ByteStream& stream, Stz2Box* stz2, StblBox* stblBox)
{
    Result result = OK;
    // connect to parent
    stblBox->mStz2= stz2;
    // Read FullBox attributes
    stz2->mVersion = stream.readUInt8();
    stz2->mFlags = stream.readUInt24();

    stream.readUInt24();
    /*
    field_size is an integer specifying the size in bits of the entries in the following table; it
    shall take the value 4, 8 or 16. If the value 4 is used, then each byte contains two values:
    entry[i]<<4 + entry[i+1]; if the sizes do not fill an integral number of bytes, the last byte is
    padded with zeros.
    */
    // 
    u_int8_t field_size = stream.readUInt8();

    TrakBox* trakBox = stblBox->mParent->mParent->mParent;
    trakBox->mStszSampleCount = stream.readUInt32();

    if (trakBox->mStszSampleCount > 0) {
        trakBox->mStszEntrySize = new u_int32_t[trakBox->mStszSampleCount];
        u_int8_t prevByte = 0;
        for (unsigned i = 0; i < trakBox->mStszSampleCount; i++) {
            if (field_size == 4) {
                if ((i % 2) == 0) {
                    prevByte = stream.readUInt8();
                    trakBox->mStszEntrySize[i] = prevByte & 0xF0;
                } else {
                    trakBox->mStszEntrySize[i] = prevByte & 0x0F;
                }
            }
            else if (field_size == 8)
                trakBox->mStszEntrySize[i] = stream.readUInt8();
            else // (field_size == 16)
                trakBox->mStszEntrySize[i] = stream.readUInt16();
        }
    }

    return result;
}
//'mdia'-->'minf'-->'stbl'-->'stco' chunk offset
Result Mp4Parser::ParseStco(ByteStream& stream, StcoBox* stco, StblBox* stblBox)
{
    Result result = OK;
    // connect to parent
    stblBox->mStco = stco;
    // Read FullBox attributes
    stco->mVersion = stream.readUInt8();
    stco->mFlags = stream.readUInt24();
    TrakBox* trakBox = stblBox->mParent->mParent->mParent;

    trakBox->mStcoEntryCount = stream.readUInt32();
    if (trakBox->mStcoEntryCount) {
        trakBox->mStcoChunkOffset = new int64_t[trakBox->mStcoEntryCount];
         if (stco->getType() == BOX_STCO) {
             for (uint32_t i = 0; i < trakBox->mStcoEntryCount; i++) {
                trakBox->mStcoChunkOffset[i] = (int64_t)stream.readUInt32();
             }
         } else { //if (stco->getType() == BOX_CO64) {
             for (uint32_t i = 0; i < trakBox->mStcoEntryCount; i++) {
                 trakBox->mStcoChunkOffset[i] = (int64_t)stream.readUInt64();
             }
         }
    }
    return result;
}

//'mdia'-->'minf'-->'stbl'-->'co64' 64-bit chunk offset
Result Mp4Parser::ParseCo64(ByteStream& stream, Co64Box* co64, StblBox* stblBox)
{
    Result result = OK;
    // connect to parent
    stblBox->mCo64 = co64;
    // Read FullBox attributes
    co64->mVersion = stream.readUInt8();
    co64->mFlags = stream.readUInt24();

    return result;
}

//-----
//'mdia'-->'minf'-->'stbl'-->'stsh' - shadow sync
Result Mp4Parser::ParseStsh(ByteStream& stream, StshBox* stsh, StblBox* stblBox)
{
    Result result = OK;
    // connect to parent
    stblBox->mStsh = stsh;
    // Read FullBox attributes
    stsh->mVersion = stream.readUInt8();
    stsh->mFlags = stream.readUInt24();

    return result;
}
//'mdia'-->'minf'-->'stbl'-->'padb'
Result Mp4Parser::ParsePadb(ByteStream& stream, PadbBox* padb, StblBox* stblBox)
{
    Result result = OK;
    // connect to parent
    stblBox->mPadb = padb;
    // Read FullBox attributes
    padb->mVersion = stream.readUInt8();
    padb->mFlags = stream.readUInt24();

    return result;
}
//'mdia'-->'minf'-->'stbl'-->'stdp'
Result Mp4Parser::ParseStdp(ByteStream& stream, StdpBox* stdp, StblBox* stblBox)
{
    Result result = OK;
    // connect to parent
    stblBox->mStdp = stdp;
    // Read FullBox attributes
    stdp->mVersion = stream.readUInt8();
    stdp->mFlags = stream.readUInt24();

    return result;
}
//'mdia'-->'minf'-->'stbl'-->'sdtp'
Result Mp4Parser::ParseSdtp(ByteStream& stream, SdtpBox* sdtp, StblBox* stblBox)
{
    Result result = OK;
    // connect to parent
    stblBox->mSdtp = sdtp;
    // Read FullBox attributes
    sdtp->mVersion = stream.readUInt8();
    sdtp->mFlags = stream.readUInt24();

    return result;
}
//'mdia'-->'minf'-->'stbl'-->'sbgp'
Result Mp4Parser::ParseSbgp(ByteStream& stream, SbgpBox* sbgp, StblBox* stblBox)
{
    Result result = OK;
    // connect to parent
    stblBox->mSbgp = sbgp;
    // Read FullBox attributes
    sbgp->mVersion = stream.readUInt8();
    sbgp->mFlags = stream.readUInt24();

    return result;
}
//-----
//'mdia'-->'minf'-->'stbl'-->'sgpd'
Result Mp4Parser::ParseSgpd(ByteStream& stream, SgpdBox* sgpd, StblBox* stblBox)
{
    Result result = OK;
    // connect to parent
    stblBox->mSgpd = sgpd;
    // Read FullBox attributes
    sgpd->mVersion = stream.readUInt8();
    sgpd->mFlags = stream.readUInt24();

    return result;
}
//'mdia'-->'minf'-->'stbl'-->'subs'
Result Mp4Parser::ParseSubs(ByteStream& stream, SubsBox* subs, StblBox* stblBox)
{
    Result result = OK;
    // connect to parent
    stblBox->mSubs = subs;
    // Read FullBox attributes
    subs->mVersion = stream.readUInt8();
    subs->mFlags = stream.readUInt24();

    return result;
}
//'mdia'-->'minf'-->'stbl'-->'saiz'
Result Mp4Parser::ParseSaiz(ByteStream& stream, SaizBox* saiz, StblBox* stblBox)
{
    Result result = OK;
    // connect to parent
    stblBox->mSaiz = saiz;
    // Read FullBox attributes
    saiz->mVersion = stream.readUInt8();
    saiz->mFlags = stream.readUInt24();

    return result;
}
//'mdia'-->'minf'-->'stbl'-->'saio'
Result Mp4Parser::ParseSaio(ByteStream& stream, SaioBox* saio, StblBox* stblBox)
{
    Result result = OK;
    // connect to parent
    stblBox->mSaio = saio;
    // Read FullBox attributes
    saio->mVersion = stream.readUInt8();
    saio->mFlags = stream.readUInt24();

    return result;
}