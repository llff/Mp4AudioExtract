/**
 *   Copyright TBD by Liangfeng, All rights reserved.
 */

#pragma once
#include <stdarg.h>
#include <cstring>
#include <iostream>
#ifdef _WIN32
typedef signed char        int8_t;
typedef short       int16_t;
typedef int         int32_t;
typedef long long  int64_t;

typedef unsigned char       u_int8_t;
typedef unsigned short      u_int16_t;
typedef unsigned int        u_int32_t;
typedef unsigned long long  u_int64_t;
#endif

const int TRACE_LEVEL_TRACE = 0;
void trace_print(const char* file,
                 const int line,
                 const char* func,
                 const unsigned level,
                 const char* payload,
                 ...);

#define TRACE(...)  //trace_print( __FILE__, __LINE__, __FUNCTION__, TRACE_LEVEL_TRACE, __VA_ARGS__)


enum Result 
{ 
    OK = 0,
    ERROR = -1,
};

inline constexpr u_int32_t FourCCToUInt32(const char* fcc)
{
    return static_cast<u_int32_t>((fcc[0] << 24) | (fcc[1] << 16) | (fcc[2] << 8) | fcc[3]);
}

enum Mp4BoxType {
    BOX_FTYP = 0x66747970,
    BOX_MOOV = 0x6D6F6F76,  // moov
    BOX_MVHD = 0x6D766864,  // mvhd
    BOX_IODS = 0x696F6473,
    BOX_UDTA = 0x75647461,

    BOX_TRAK = 0x7472616B, // trak
    BOX_TKHD = 0x746b6864,
    BOX_TREF = 0x74726566,
    BOX_TRGR = 0x74726772,
    BOX_EDTS = 0x65647473,
    BOX_ELST = 0x656C7374,

    // Media box
    BOX_MDIA = 0x6D646961,  // mdia
    BOX_MDHD = 0x6D646864,  // mdhd
    BOX_HDLR = 0x68646C72,
    // MINF box
    BOX_MINF = 0x6D696E66,
    BOX_VMHD = 0x766D6864,
    BOX_SMHD = 0x736D6864,
    BOX_HMHD = 0x686D6864,
    BOX_NMHD = 0x6E6D6864,
    BOX_DINF = 0x64696E66,
    BOX_DREF = 0x64726566,
    BOX_URL  = 0x75726C20,
    BOX_URN  = 0x75726E20,
    // sample box
    BOX_STBL = 0x7374626C,
    BOX_STSD = 0x73747364,
    BOX_STTS = 0x73747473,
    BOX_CTTS = 0x63747473,
    BOX_CSLG = 0x63736c67,
    BOX_STSC = 0x73747363,
    BOX_STSZ = 0x7374737A,
    BOX_STZ2 = 0x73747A32,
    BOX_STCO = 0x7374636F,
    BOX_CO64 = 0x636F3634,
    BOX_STSS = 0x73747373,
    BOX_STSH = 0x73747368,
    BOX_PADB = 0x70616462,
    BOX_STDP = 0x73746470,
    BOX_SDTP = 0x73647470,
    BOX_SBGP = 0x73626770,
    BOX_SGPD = 0x73677064,
    BOX_SUBS = 0x73756273,
    BOX_SAIZ = 0x7361697A,
    BOX_SAIO = 0x7375626F,

    //
    BOX_ESDS = 0x65736473,      // elementary stream descriptor box

    //
    BOX_MOOF = 0x6D6F6F66,
    BOX_MFHD = 0x6D666864,
    BOX_TRAF = 0x74726166,
    BOX_TFHD = 0x74666866,
    BOX_TRUN = 0x7472756E,
    BOX_TFDT = 0x74666474,

    BOX_MFRA = 0x6D667261,
    BOX_TFRA = 0x74667261,
    BOX_MFRO = 0x6d66726f,

    BOX_MDAT = 0x6D646174,
    BOX_META = 0x6D657461,
    BOX_ILST = 0x696C7374,

    BOX_MECO = 0x6D65636F,
    BOX_MERE = 0x6D657265,

    BOX_SIDX = 0x73696478,
    BOX_SSIX = 0x73736978,
    BOX_PRFT = 0x70726674,

    BOX_FREE = 0x66726565,
    BOX_SKIP = 0x736B6970,
    BOX_UUID = 0x75756964

};
enum ContainerProfile {
    PROF_UNKNOWN = 0,
    PROF_AVI_OpenDML = 2,
    PROF_MPEG_PS_1 = 3,
    PROF_MPEG_PS_2 = 4,
    PROF_ISOBMF_MOV = 8,
    PROF_ISOBMF_MP4 = 9,
    PROF_ISOBMF_3GP = 10,
    PROF_ISOBMF_MJP = 11,
    PROF_WAVE_AMB = 16,
    PROF_WAVE_RF64 = 17,
    PROF_WAVE_BWFv1 = 18,
    PROF_WAVE_BWFv2 = 19,
    PROF_WAVE_BWF64 = 20,
    PROF_MKV_MATROSKA = 24,
    PROF_MKV_WEBM = 25
};

enum HandlerType {
    HANDLER_AUDIO = FourCCToUInt32("soun"),
    HANDLER_VIDEO = FourCCToUInt32("vide"),
    HANDLER_HINT = FourCCToUInt32("hint"),
    HANDLER_META = FourCCToUInt32("meta"),
    HANDLER_TMCD = FourCCToUInt32("tmcd"),
    HANDLER_SUBT = FourCCToUInt32("subt"), 
    HANDLER_SBTL = FourCCToUInt32("sbtl"),
    HANDLER_TEXT = FourCCToUInt32("text")
};

enum Codec {
    CODEC_UNKNOWN = 0,
    // audio
    CODEC_MPEG_L1 = 1,  // MP1, or MPEG 1/2 Audio Layer I
    CODEC_MPEG_L2 = 2,  // MP2, or MPEG 1/2 Audio Layer II
    CODEC_MPEG_L3 = 3,  // MP3, or MPEG 1/2 Audio Layer III
    CODEC_AAC = 4,      // Advanced Audio Coding, or MPEG-2 Part 7 and MPEG-4 Part 3
    CODEC_AC2 = 5,      //
    CODEC_AC3 = 6,      // AC-3
    CODEC_AC4 = 7,      // AC-4
    CODEC_LPCM = 8,     // LPCM

};
enum MediaType {
    MEDIA_UNKNOWN = 0,
    MEDIA_AUDIO,
    MEDIA_VIDEO,
    MEDIA_VIDEO_SYNC,
};
// NOTE: Big Endian
inline u_int64_t BytesToUInt64(const u_int8_t* bytes)
{
    return (((u_int64_t)bytes[0]) << 56) | (((u_int64_t)bytes[1]) << 48) |
           (((u_int64_t)bytes[2]) << 40) | (((u_int64_t)bytes[3]) << 32) |
           (((u_int64_t)bytes[4]) << 24) | (((u_int64_t)bytes[5]) << 16) |
           (((u_int64_t)bytes[6]) << 8) | (((u_int64_t)bytes[7]));
}

inline u_int32_t BytesToUInt32(const u_int8_t* bytes)
{
    return (((u_int32_t)bytes[0]) << 24) | (((u_int32_t)bytes[1]) << 16) |
           (((u_int32_t)bytes[2]) << 8) | (((u_int32_t)bytes[3]));
}

inline int32_t BytesToInt32(const u_int8_t* bytes) { return BytesToUInt32(bytes); }

inline u_int32_t BytesToUInt24(const u_int8_t* bytes)
{
    return (((u_int32_t)bytes[0]) << 16) | (((u_int32_t)bytes[1]) << 8) | (((u_int32_t)bytes[2]));
}

inline u_int16_t BytesToUInt16(const u_int8_t* bytes)
{
    return (((u_int16_t)bytes[0]) << 8) | (((u_int16_t)bytes[1]));
}

inline int16_t BytesToInt16(const u_int8_t* bytes) { return (int16_t)BytesToUInt16(bytes); }

inline
double BytesToDouble(const u_int8_t* bytes)
{
    u_int64_t value = BytesToUInt64(bytes);
    void*   vValue = reinterpret_cast<void*>(&value);
    double* dValue = reinterpret_cast<double*>(vValue);

    return *dValue;
}


