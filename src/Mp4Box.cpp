/**
 *   Copyright TBD by Liangfeng, All rights reserved.
 */

#include "Mp4Box.h"

///Mp4Box///////////////////////////////////////////////////////////////////////
Mp4Box::Mp4Box(u_int64_t size, u_int32_t type) : mSize(size), mType(type), mIsLargeSize(false)
{
    /* 
    aligned(8) class FullBox(unsigned int(32) boxtype, unsigned int(8) v, bit(24) f)
        extends Box(boxtype) {
        unsigned int(8) version = v;
        bit(24) flags = f;
    }
    */
    mVersion = 0xFF;
    mFlags = 0xFFFFFFFF;
}
Mp4Box::Mp4Box() :mSize(0), mType(0), mIsLargeSize(false)
{
}

Mp4Box::~Mp4Box() 
{
}

u_int64_t    Mp4Box::getSize() const { return mSize; }
u_int32_t    Mp4Box::getType() const { return mType; }
bool         Mp4Box::isLargeSize() const { return mIsLargeSize;}

///FtyeBox///////////////////////////////////////////////////////////////////////
FtypBox::FtypBox(u_int64_t size)
    : Mp4Box(size, BOX_FTYP), mProfile(PROF_UNKNOWN), mMajorBrand(0),
      mMinorVersion(0) 
{
  memset(mCompatiblBrands, 0, sizeof(u_int32_t) * 16);
}

FtypBox::~FtypBox() 
{
}

///MoovBox///////////////////////////////////////////////////////////////////////
void MoovBox::addTrack(TrakBox* trak) { 
    //
    mTracks.push_back(trak); 
}

MoovBox::~MoovBox() {

    delete mMvhd;
    for (auto &t : mTracks) {
        delete t;
    }
}

///TraxBox///////////////////////////////////////////////////////////////////////
TrakBox::TrakBox(int64_t size, MoovBox* parent)
    : Mp4Box(size, BOX_TRAK), mParent(parent), mTkhd(nullptr)
{

    //
    mEdtsBox = nullptr;
    mMdiaBox = nullptr;
    mHandlerType = 0;
    memset(mTrackName, 0, 256);

    mCodec = CODEC_UNKNOWN;
    mChannelCnt = 0;
    mSampleSizeInBit = 0;
    mSamplerate = 0; //in HZ

    //
    //mSampleCount = 0;
    //mSampleType = nullptr;
    //mSampleSize = nullptr;
    //mSampleOffset = nullptr;

    // stts
    mSttsEntryCount = 0;
    mSttsSampleCount = nullptr;
    mSttsSampleDelta = nullptr;
    // ctts
    mCttsEntryCount = 0;
    mCttsSampleCount = nullptr;
    mCttsSampleOffset = nullptr;

    // stss
    mStssEntryCount = 0;
    mStssSampleNumber = nullptr;
    // stsc
    mStscEntryCount = 0;
    mStscFirstChunk = nullptr;
    mStscSamplesPerChunk = nullptr;
    mStscSampleDescriptionIndex = nullptr;

    //stsz/stz2
    mStszSampleCount = 0;
    mStszSampleSize = 0;
    mStszEntrySize = nullptr;

    // stco/co64
    mStcoEntryCount = 0;
    mStcoChunkOffset = nullptr;

}

TrakBox::~TrakBox() {
    delete mTkhd;
    delete mEdtsBox;
    delete mMdiaBox;
    for (auto &m : mMedia) {
        delete m;
    }

    delete mSttsSampleCount;
    delete mSttsSampleDelta;
    //TODO:
    //delete mSampleType;
    //delete mSampleSize;
    //delete mSampleOffset;

    //Stts
    delete mSttsSampleCount;
    delete mSttsSampleDelta;
    // ctts
    delete mCttsSampleCount;
    delete mCttsSampleOffset;

    // stsc
    delete mStscFirstChunk;
    delete mStscSamplesPerChunk;
    delete mStscSampleDescriptionIndex;

    delete mStszEntrySize;
    delete mStcoChunkOffset;

}

///StblBox///////////////////////////////////////////////////////////////////////
StblBox::StblBox(int64_t size, MinfBox* parent) : Mp4Box(size, BOX_STBL), mParent(parent)
{
    //
    mStsd = nullptr;
    mStts = nullptr;
    mCtts = nullptr;
    mCslg = nullptr;
    mStsc = nullptr;

    mStsz = nullptr;
    mStz2 = nullptr;
    mStco = nullptr;
    mCo64 = nullptr;
    mStss = nullptr;

    mStsh = nullptr;
    mPadb = nullptr;
    mStdp = nullptr;
    mSdtp = nullptr;
    mSbgp = nullptr;

    mSgpd = nullptr;
    mSubs = nullptr;
    mSaiz = nullptr;
    mSaio = nullptr;
}
StblBox::~StblBox() 
{
    //
    delete mStsd;
    delete mStts;
    delete mCtts;
    delete mCslg;
    delete mStsc;
    //
    delete mStsz;
    delete mStz2;
    delete mStco;
    delete mCo64;
    delete mStss;
    //
    delete mStsh;
    delete mPadb;
    delete mStdp;
    delete mSdtp;
    delete mSbgp;
    //
    delete mSgpd;
    delete mSubs;
    delete mSaiz;
    delete mSaio;
}