#pragma once
//ISO-14496 part 12 (2012)
/*
ftyp
pdin
moov
----mvhd
----trak
--------tkhd
--------edts
--------mdia
------------mdhd
------------hdlr
----------------minf
--------------------vmhd
--------------------smhd
--------------------hmhd
--------------------dinf
------------------------dref
--------------------stbl
------------------------stsd
------------------------stts
------------------------ctts
------------------------cslg
------------------------stsc
------------------------stsz
------------------------stz2
------------------------stco
------------------------co64
------------------------stss
------------------------stsh
------------------------padb
------------------------stdp
------------------------sdtp
------------------------sbgp
------------------------sgpd
------------------------subs
------------------------saiz
------------------------saio
------------udta
--------mvex
------------mehd
------------trex
------------leva
----moof
--------mfhd
--------traf
------------tfhd
------------trun
------------sbgp
------------sgpd
------------subs
*/
/**
 *   Copyright TBD by Liangfeng, All rights reserved.
 */
#include <list>
#include "Common.h"
#include "ByteStream.h"

//Box header
class Mp4Box {

public:
    explicit Mp4Box(u_int64_t size, u_int32_t type);
    u_int64_t getSize() const;
    u_int32_t getType() const;
    bool isLargeSize() const;
    //virtual std::list<Mp4Box*>& getChildrenBoxes() const = 0;
    virtual ~Mp4Box();
private:
    u_int64_t mSize;
    u_int32_t mType;
    bool mIsLargeSize;
	u_int8_t mVersion;
	u_int32_t mFlags; //actually 24bit ISO-14496-12
    //    std::list<Mp4Box*> mBoxes;
    u_int64_t mEndPos;
    Mp4Box();
	friend class Mp4Parser;
};


/*
aligned(8) class FileTypeBox extends Box(�ftyp�)
{
    unsigned int(32) major_brand;
    unsigned int(32) minor_version;
    unsigned int(32) compatible_brands[];  // to end of the box
}*/
class FtypBox : public Mp4Box
{
public:
    explicit FtypBox(u_int64_t size);
    ~FtypBox();
//private:
	ContainerProfile mProfile;
    u_int32_t mMajorBrand;
    u_int32_t mMinorVersion;
    u_int32_t mCompatiblBrands[16];
};

/**
  * aligned(8) class MovieBox extends Box(�moov�) {}
*/
class TrakBox;
class MvhdBox;
class MoovBox : public Mp4Box
{
public:
    explicit MoovBox(int64_t size) : Mp4Box(size, BOX_MOOV) {}
    std::list<TrakBox*>& getTrakBoxes() { return mTracks; }
    u_int32_t GetTimeScale() { return mTimeScale; }
    void addTrack(TrakBox* trak);
    ~MoovBox();
//private:
    // methods
    // members
    MvhdBox* mMvhd;
    std::list<TrakBox*> mTracks;
    u_int32_t mTimeScale;
};

/*
/*
aligned(8) class MovieHeaderBox extends FullBox(�mvhd�, version, 0) {
if (version==1) {
unsigned int(64) creation_time;
unsigned int(64) modification_time;
unsigned int(32) timescale;
unsigned int(64) duration;
} else { // version==0
unsigned int(32) creation_time;
unsigned int(32) modification_time;
unsigned int(32) timescale;
unsigned int(32) duration;
}
template int(32) rate = 0x00010000; // typically 1.0
template int(16) volume = 0x0100; // typically, full volume
const bit(16) reserved = 0;
const unsigned int(32)[2] reserved = 0;
template int(32)[9] matrix =
{ 0x00010000,0,0,0,0x00010000,0,0,0,0x40000000 };
// Unity matrix
bit(32)[6] pre_defined = 0;
unsigned int(32) next_track_ID;
}
*/

class MvhdBox : public Mp4Box {

public:
    explicit MvhdBox(int64_t size, MoovBox* parent) : Mp4Box(size, BOX_MVHD), mParent(parent) {
        mCreationTime = 0;
        mModificationTime = 0;
        mTimescale = 0;
        mDuration = 0;
        mNextTrackId = 0;
    }

//private:
    MoovBox* mParent;
    //
    u_int64_t mCreationTime;
    u_int64_t mModificationTime;
    u_int32_t mTimescale;
    u_int64_t mDuration;
    u_int32_t mNextTrackId;
};

struct TrackInfo {

public:
    TrackInfo() {
        mHandlerType = 0;
        mCodec = CODEC_UNKNOWN;
        mSampleCount = 0;
        mSampleOffset = nullptr;
        mSampleType = nullptr;
        mSampleSize = nullptr;
    }
    u_int32_t mHandlerType;   //input from hdlr box
    Codec mCodec;
    int64_t* mSampleOffset;

    // calculated
    u_int32_t mSampleCount;
    u_int32_t* mSampleType;
    u_int32_t* mSampleSize;
};

class TkhdBox;
class MeidaBox;
class EdtsBox;
class MdiaBox;
class TrakBox : public Mp4Box
{
public:
    explicit TrakBox(int64_t size, MoovBox *parent);
    ~TrakBox();
//private:
    // parent
    MoovBox* mParent;
    //Children
    TkhdBox* mTkhd;
    EdtsBox* mEdtsBox;
    MdiaBox* mMdiaBox;
    std::list<MeidaBox*> mMedia;
    //attribute
    u_int32_t mHandlerType;   //input from hdlr box
    char     mTrackName[256]; //input from hdlr box

    // sample attributes
    Codec mCodec;
    u_int16_t mChannelCnt;
    u_int16_t mSampleSizeInBit;
    u_int16_t mSamplerate;

    //stts
    u_int32_t   mSttsEntryCount;
    u_int32_t*  mSttsSampleCount;
    u_int32_t*  mSttsSampleDelta;
    //ctts
    u_int32_t   mCttsEntryCount;
    u_int32_t*  mCttsSampleCount;
    int64_t*    mCttsSampleOffset;

    // stss
    u_int32_t   mStssEntryCount;
    u_int32_t*  mStssSampleNumber;

    //stsc
    u_int32_t   mStscEntryCount;
    u_int32_t*  mStscFirstChunk;
    u_int32_t*  mStscSamplesPerChunk;
    u_int32_t*  mStscSampleDescriptionIndex;

    // stsz / stz2
    u_int32_t   mStszSampleCount;
    u_int32_t   mStszSampleSize;
    u_int32_t*  mStszEntrySize;

    // stco / co64
    u_int32_t   mStcoEntryCount;
    int64_t*    mStcoChunkOffset;

};

/*!
aligned(8) class TrackHeaderBox
extends FullBox(�tkhd�, version, flags){
if (version==1) {
unsigned int(64) creation_time;
unsigned int(64) modification_time;
unsigned int(32) track_ID;
const unsigned int(32) reserved = 0;
unsigned int(64) duration;
} else { // version==0
unsigned int(32) creation_time;
unsigned int(32) modification_time;
unsigned int(32) track_ID;
const unsigned int(32) reserved = 0;
unsigned int(32) duration;
}
const unsigned int(32)[2] reserved = 0;
template int(16) layer = 0;
template int(16) alternate_group = 0;
template int(16) volume = {if track_is_audio 0x0100 else 0};
const unsigned int(16) reserved = 0;
template int(32)[9] matrix=
{ 0x00010000,0,0,0,0x00010000,0,0,0,0x40000000 };
// unity matrix
unsigned int(32) width;
unsigned int(32) height;
}
*/
class TkhdBox : public Mp4Box {

public:
    explicit TkhdBox(int64_t size, TrakBox *parent)
        : Mp4Box(size, BOX_TKHD), mParent(parent) {
        //
        mCreationTime = 0;
        mModificationTime = 0;
        mId = 0;
        mReserved = 0;
        mDuration = 0;
        mWidth = 0;
        mHeight = 0;
    }
//private:
    TrakBox *mParent;
    //attributes
    u_int64_t mCreationTime;
    u_int64_t mModificationTime;
    u_int32_t mId;
    u_int32_t mReserved;
    u_int64_t mDuration;
    u_int32_t mWidth;
    u_int32_t mHeight;
};

class ElstBox;
class EdtsBox : public Mp4Box {
public:
    explicit EdtsBox(int64_t size, TrakBox* parent) : Mp4Box(size, BOX_EDTS), mParent(parent) {
        //
        mElst = nullptr;
    }
    ~EdtsBox() {
        //
        delete mElst;
    }
//private:
    TrakBox* mParent;
    ElstBox* mElst;
};

/*!
aligned(8) class EditListBox extends FullBox(�elst�, version, 0) {
unsigned int(32) entry_count;
for (i=1; i <= entry_count; i++) {
if (version==1) {
unsigned int(64) segment_duration;
int(64) media_time;
} else { // version==0
unsigned int(32) segment_duration;
int(32) media_time;
}
int(16) media_rate_integer;
int(16) media_rate_fraction = 0;
}
}
*/
class ElstBox : public Mp4Box
{
public:
    explicit ElstBox(int64_t size, EdtsBox* parent) : Mp4Box(size, BOX_ELST), mParent(parent)
    {
        //
        mSegmentDuration = 0;
        mMediaTime = 0;
        mMediaRateInterger = 0;
        mMediaRateFraction = 0;       
    }
    ~ElstBox() {
        //
    }
// private:
    EdtsBox* mParent;
    u_int64_t mSegmentDuration;
    int64_t mMediaTime;
    int16_t mMediaRateInterger;
    int16_t mMediaRateFraction;
};

class MdhdBox;
class HdlrBox;
class MinfBox;
class MdiaBox : public Mp4Box
{
public:
    explicit MdiaBox(int64_t size, TrakBox *parent)
      : Mp4Box(size, BOX_MDIA), mParent(parent), mMdhd(nullptr) {
        //
        mMdhd = nullptr;
        mHdlr = nullptr;
        mMinf = nullptr;
    }
    ~MdiaBox() {
        delete mMdhd;
        delete mHdlr;
        delete mMinf;
    }
    //Parent
    TrakBox *mParent;
    //Children
    MdhdBox* mMdhd;
    HdlrBox* mHdlr;
    MinfBox* mMinf;
};
/*
    aligned(8) class MediaHeaderBox extends FullBox(�mdhd�, version, 0) {
    if (version==1) {
        unsigned int(64) creation_time;
        unsigned int(64) modification_time;
        unsigned int(32) timescale;
        unsigned int(64) duration;
    } else { // version==0
        unsigned int(32) creation_time;
        unsigned int(32) modification_time;
        unsigned int(32) timescale;
        unsigned int(32) duration;
    }
    bit(1) pad = 0;
    unsigned int(5)[3] language; // ISO-639-2/T language code
    unsigned int(16) pre_defined = 0;
    }
*/
class MdhdBox : public Mp4Box {
public:
    explicit MdhdBox(int64_t size, MdiaBox* parent) : Mp4Box(size, BOX_MDHD), mParent(parent)
    {
        //
        mCreateTime = 0;
        mModifictioniTime = 0;
        mTimescale = 0;
        mCreateTime = 0;
        mDuration = 0;
    }
    // Parent
    MdiaBox* mParent;
    //Children
    u_int64_t mCreateTime;
    u_int64_t mModifictioniTime;
    u_int32_t mTimescale;
    u_int64_t mDuration;

    u_int8_t mLanguage[3]; // 5 bit in each

};
/*
aligned(8) class HandlerBox extends FullBox(�hdlr�, version = 0, 0) {
unsigned int(32) pre_defined = 0;
unsigned int(32) handler_type;
const unsigned int(32)[3] reserved = 0;
string name;
}
*/
class HdlrBox : public Mp4Box
{
public:
    explicit HdlrBox(int64_t size, MdiaBox* parent) : Mp4Box(size, BOX_HDLR), mParent(parent)
    {
        //
    }
    // Parent
    MdiaBox* mParent;
};

class VmhdBox;
class SmhdBox;
class HmhdBox;
class NmhdBox;
class DinfBox;
class StblBox;
class MinfBox : public Mp4Box
{
public:
    explicit MinfBox(int64_t size, MdiaBox* parent) : Mp4Box(size, BOX_MINF), mParent(parent)
    {
        //
        mVmhd = nullptr;
        mSmhd = nullptr;
        mHmhd = nullptr;
        mNmhd = nullptr;
        mDinf = nullptr;
    }
    ~MinfBox()
    {
        delete mVmhd;
        delete mSmhd;
        delete mHmhd;
        delete mNmhd;
        delete mDinf;
    }
    // Parent
    MdiaBox* mParent;
    //children
    VmhdBox* mVmhd;
    SmhdBox* mSmhd;
    HmhdBox* mHmhd;
    NmhdBox* mNmhd;
    DinfBox* mDinf;
    //container
    StblBox* mStblBox;
};

/*
aligned(8) class VideoMediaHeaderBox extends FullBox(�vmhd�, version = 0, 1)
{
    template unsigned int(16) graphicsmode = 0;  // copy, see below
    template unsigned int(16)[3] opcolor = {0, 0, 0};
}
*/
class VmhdBox : public Mp4Box
{
public:
    explicit VmhdBox(int64_t size, MinfBox* parent) : Mp4Box(size, BOX_VMHD), mParent(parent)
    {
        //
        mGraphicsmode = 0;
        mOpcolor[0] = 0;
        mOpcolor[1] = 0;
        mOpcolor[2] = 0;
    }
    // Parent
    MinfBox* mParent;
    // children
    u_int16_t mGraphicsmode;
    u_int16_t mOpcolor[3];
};
/*
aligned(8) class SoundMediaHeaderBox
extends FullBox(�smhd�, version = 0, 0) {
template int(16) balance = 0;
const unsigned int(16) reserved = 0;
}
*/
class SmhdBox : public Mp4Box
{
public:
    explicit SmhdBox(int64_t size, MinfBox* parent) : Mp4Box(size, BOX_SMHD), mParent(parent)
    {
        //
        mBalance = 0;
    }
    // Parent
    MinfBox* mParent;
    //Children
    int16_t mBalance;

};

/*
aligned(8) class HintMediaHeaderBox
extends FullBox(�hmhd�, version = 0, 0) {
unsigned int(16) maxPDUsize;
unsigned int(16) avgPDUsize;
unsigned int(32) maxbitrate;
unsigned int(32) avgbitrate;
unsigned int(32) reserved = 0;
}

*/
class HmhdBox : public Mp4Box
{
public:
    explicit HmhdBox(int64_t size, MinfBox* parent) : Mp4Box(size, BOX_MDHD), mParent(parent)
    {
        //
    }
    // Parent
    MinfBox* mParent;
    //Children
    u_int16_t mMaxPduSize;
    u_int16_t mAvgPduSize;

    u_int32_t mMaxBitrate;
    u_int32_t mAvgBitrate;
    //u_int32_t reserved;
};
/*
aligned(8) class NullMediaHeaderBox
extends FullBox(�nmhd�, version = 0, flags) {
}
*/
class NmhdBox : public Mp4Box
{
public:
    explicit NmhdBox(int64_t size, MinfBox* parent) : Mp4Box(size, BOX_NMHD), mParent(parent)
    {
        //
    }
    // Parent
    MinfBox* mParent;
};
class DrefBox;
class DinfBox : public Mp4Box
{
public:
    explicit DinfBox(int64_t size, MinfBox* parent) : Mp4Box(size, BOX_DINF), mParent(parent)
    {
        //
        mDref = nullptr;
    }
    ~DinfBox() {
        delete mDref;
    }
    // Parent
    MinfBox* mParent;
    //Child
    DrefBox* mDref;
};

/*
aligned(8) class DataReferenceBox
extends FullBox(�dref�, version = 0, 0) {
        unsigned int(32) entry_count;
        for (i=1; i <= entry_count; i++) {
            DataEntryBox(entry_version, entry_flags) data_entry;
        }
}
*/
class DrefBox : public Mp4Box
{
public:
    explicit DrefBox(int64_t size, DinfBox* parent) : Mp4Box(size, BOX_DREF), mParent(parent)
    {
        //
        mEntryCount= 0;
    }
    ~DrefBox() {
        //
    }
    // Parent
    DinfBox* mParent;
    //Children
    u_int32_t mEntryCount;

};

///sample boxes - 19 sub boxes
class StsdBox;
class SttsBox;
class CttsBox;
class CslgBox;
class StscBox;
class StszBox;
class Stz2Box;
class StcoBox;
class Co64Box;
//
class StssBox;
class StshBox;
class PadbBox;
class StdpBox;
class SdtpBox;
class SbgpBox;
class SgpdBox;
class SubsBox;
class SaizBox;
class SaioBox;

class StblBox : public Mp4Box
{
public:
    explicit StblBox(int64_t size, MinfBox* parent);
    ~StblBox();
    // Parent
    MinfBox* mParent;
    //Children
//private:
    StsdBox* mStsd;
    SttsBox* mStts;
    CttsBox* mCtts;
    CslgBox* mCslg;
    StscBox* mStsc;
    //
    StszBox* mStsz;
    Stz2Box* mStz2;
    StcoBox* mStco;
    Co64Box* mCo64;
    StssBox* mStss;
    //
    StshBox* mStsh;
    PadbBox* mPadb;
    StdpBox* mStdp;
    SdtpBox* mSdtp;
    SbgpBox* mSbgp;
    //
    SgpdBox* mSgpd;
    SubsBox* mSubs;
    SaizBox* mSaiz;
    SaioBox* mSaio;
};

class StsdBox : public Mp4Box
{
public:
    explicit StsdBox(int64_t size, StblBox* parent) : Mp4Box(size, BOX_STSD), mParent(parent) {
        //
    }
    StblBox* mParent;
};

class SttsBox : public Mp4Box
{
public:
    explicit SttsBox(int64_t size, StblBox* parent) : Mp4Box(size, BOX_STTS), mParent(parent)
    {
        //
        mEntryCount = 0;
    }
    StblBox* mParent;
    //attributes
    u_int32_t mEntryCount;
};
class CttsBox : public Mp4Box
{
public:
    explicit CttsBox(int64_t size, StblBox* parent) : Mp4Box(size, BOX_CTTS), mParent(parent)
    {
        //
        mEntryCount = 0;
    }
    StblBox* mParent;
    //
    u_int32_t mEntryCount;
};
//
class StssBox : public Mp4Box
{
public:
    explicit StssBox(int64_t size, StblBox* parent) : Mp4Box(size, BOX_STSS), mParent(parent)
    {
        //
        mEntryCount = 0;
    }
    StblBox* mParent;
    //att
    u_int32_t mEntryCount;
};

/*
/*
class CompositionToDecodeBox extends FullBox(�cslg�, version=0, 0) {
signed int(32) compositionToDTSShift;
signed int(32) leastDecodeToDisplayDelta;
signed int(32) greatestDecodeToDisplayDelta;
signed int(32) compositionStartTime;
signed int(32) compositionEndTime;
}
*/

class CslgBox : public Mp4Box
{
public:
    explicit CslgBox(int64_t size, StblBox* parent) : Mp4Box(size, BOX_CSLG), mParent(parent)
    {
        //
    }
    StblBox* mParent;
    //attributes
    int32_t mCompositionToDTSShift;
    int32_t mLeastDecodeToDisplayDelta;
    int32_t mGreatestDecodeToDisplayDelta;
    int32_t mCompositionStartTime;
    int32_t mCompositionEndTime;
};
class StscBox : public Mp4Box
{
public:
    explicit StscBox(int64_t size, StblBox* parent) : Mp4Box(size, BOX_STSC), mParent(parent)
    {
        //
    }
    StblBox* mParent;
    // attr
    u_int32_t mEntryCount;
}
;
class StszBox : public Mp4Box
{
public:
    explicit StszBox(int64_t size, StblBox* parent) : Mp4Box(size, BOX_STSZ), mParent(parent)
    {
        //
    }
    StblBox* mParent;
};
class Stz2Box : public Mp4Box
{
public:
    explicit Stz2Box(int64_t size, StblBox* parent) : Mp4Box(size, BOX_STZ2), mParent(parent)
    {
        //
    }
    StblBox* mParent;
};
class StcoBox : public Mp4Box
{
public:
    explicit StcoBox(int64_t size, StblBox* parent, u_int32_t type) : Mp4Box(size, type), mParent(parent)
    {
        //
    }
    StblBox* mParent;
};
class Co64Box : public Mp4Box
{
public:
    explicit Co64Box(int64_t size, StblBox* parent) : Mp4Box(size, BOX_CO64), mParent(parent)
    {
        //
    }
    StblBox* mParent;
};

class StshBox : public Mp4Box
{
public:
    explicit StshBox(int64_t size, StblBox* parent) : Mp4Box(size, BOX_STSH), mParent(parent)
    {
        //
    }
    StblBox* mParent;
};
class PadbBox : public Mp4Box
{
public:
    explicit PadbBox(int64_t size, StblBox* parent) : Mp4Box(size, BOX_PADB), mParent(parent)
    {
        //
    }
    StblBox* mParent;
};
class StdpBox : public Mp4Box
{
public:
    explicit StdpBox(int64_t size, StblBox* parent) : Mp4Box(size, BOX_STDP), mParent(parent)
    {
        //
    }
    StblBox* mParent;
};
class SdtpBox : public Mp4Box
{
public:
    explicit SdtpBox(int64_t size, StblBox* parent) : Mp4Box(size, BOX_SDTP), mParent(parent)
    {
        //
    }
    StblBox* mParent;
};
class SbgpBox : public Mp4Box
{
public:
    explicit SbgpBox(int64_t size, StblBox* parent) : Mp4Box(size, BOX_SBGP), mParent(parent)
    {
        //
    }
    StblBox* mParent;
};
class SgpdBox : public Mp4Box
{
public:
    explicit SgpdBox(int64_t size, StblBox* parent) : Mp4Box(size, BOX_SGPD), mParent(parent)
    {
        //
    }
    StblBox* mParent;
};
class SubsBox : public Mp4Box
{
public:
    explicit SubsBox(int64_t size, StblBox* parent) : Mp4Box(size, BOX_SUBS), mParent(parent)
    {
        //
    }
    StblBox* mParent;
};
class SaizBox : public Mp4Box
{
public:
    explicit SaizBox(int64_t size, StblBox* parent) : Mp4Box(size, BOX_SAIZ), mParent(parent)
    {
        //
    }
    StblBox* mParent;
};
class SaioBox : public Mp4Box
{
public:
    explicit SaioBox(int64_t size, StblBox* parent) : Mp4Box(size, BOX_SAIO), mParent(parent)
    {
        //
    }
    StblBox* mParent;
};