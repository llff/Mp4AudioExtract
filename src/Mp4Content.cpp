/**
 *   Copyright TBD by Liangfeng, All rights reserved.
 */
#include <stdio.h>
#include "Mp4Content.h"
#include "Mp4Box.h"
Mp4Content::Mp4Content(const char* url) : 
    mMoovBox(nullptr), 
    mFtypBox(nullptr)
{
    if (url)
    {
        strncpy(mFileUrl, url, strlen(url) > (MAX_URL_SIZE-1)? (MAX_URL_SIZE-1): strlen(url));
    }
}

Mp4Content::~Mp4Content() {
    if (mMoovBox) {
        delete mMoovBox;
    }
    if(mFtypBox) {
        delete mFtypBox;
    }
}
MoovBox* Mp4Content::getMoovBox() const { return mMoovBox; }
FtypBox* Mp4Content::getFtypBox() const { return mFtypBox; }
void Mp4Content::setMoovBox(MoovBox *moov) { mMoovBox = moov; }
void Mp4Content::setFtypBox(FtypBox *ftyp){ mFtypBox = ftyp; }