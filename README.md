# Mp4AudioExtract

Mp4AudioExtract is a simple tool used to extract audio track from mp4 video content, made by C++

  - Under developement
  - Only supports download from HTTP and output to stdout

# Build on Ubuntu
Before build the tool, the GCC with G++ development tool should be installed
```sh
# sudo apt-get install libcurl
# mkdir cmakebuild
# cd cmakebuild
# cmake ..
# make
```
# How to run
```sh
# ./Mp4AudioExtract <MP4_HTTP_URL> | <audio_player>
```
Example with [mpg321](https://packages.ubuntu.com/trusty/mpg321) audio player tool on Ubuntu
```sh
# ./Mp4AudioExtract http://distribution.bbb3d.renderfarming.net/video/mp4/bbb_sunflower_native_60fps_stereo_abl.mp4 | mpg321 -
```

